﻿using System.Collections;

namespace DataBasedCollection
{

    public class CachedListElement<T>
    {
        public T Value;
        public int Counter;

        public CachedListElement(T value)
        {
            Value = value;
        }
    }

    public class CachedRestrictedList<T> : IEnumerable<T>
    {
        private readonly int _maxCapacity;
        private readonly List<CachedListElement<T>> _list = new();

        public CachedRestrictedList(int maxCapacity)
        {
            _maxCapacity = maxCapacity;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _list.Select(e => e.Value).GetEnumerator();
        }

        public void Add(T item)
        {
            if (_list.Count < _maxCapacity)
                _list.Add(new CachedListElement<T>(item));
            else
            {
                _list.Remove(_list.MinBy(x => x.Counter) ?? throw new InvalidOperationException());
                _list.Add(new CachedListElement<T>(item));
            }
        }

        public bool CachedOr(Func<T, bool> func)
        {
            var or = false;
            foreach (var e in _list)
            {
                if (func(e.Value))
                {
                    or = true;
                    e.Counter += 1;
                }
                else
                {
                    e.Counter -= 1;
                }
            }

            return or;
        }

        public IEnumerable<T> CachedGetElementsOrNull(Func<T, bool> func)
        {
            var elements = new List<T>();
            foreach (var e in _list)
            {
                if (func(e.Value))
                {
                    elements.Add(e.Value);
                    e.Counter++;
                }
                else
                {
                    e.Counter -= 1;
                }
            }

            return elements;
        }

        public void RemoveIfTrue(Func<T, bool> func)
        {
            foreach (var element in _list.Where(x => func(x.Value)))
            {
                _list.Remove(element);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}