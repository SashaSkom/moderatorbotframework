﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataBasedCollection
{
    public abstract class HavingKey<TKey>
    {
        public virtual TKey Key { get; set; }
    }
}
