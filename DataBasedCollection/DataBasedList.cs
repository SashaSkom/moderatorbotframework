﻿using Microsoft.EntityFrameworkCore;

namespace DataBasedCollection;

public class DataBasedList<TEntity, TKey> : ISafeCollection<TEntity, TKey>
    where TEntity : HavingKey<TKey>
{
    private readonly string _connectionString;

    public void Remove(TEntity entry)
    {
        var db = new DataBaseContext<TEntity>(_connectionString);
        db.Remove(entry);
        db.SaveChanges();
    }

    public async void RemoveAsync(TEntity entry)
    {
        var db = new DataBaseContext<TEntity>(_connectionString);
        db.Remove(entry);
        await db.SaveChangesAsync();
    }

    public async void AddAsync(TEntity entry)
    {
        var db = new DataBaseContext<TEntity>(_connectionString);
        if (Find(entry.Key, out var result))
        {
            throw new ArgumentException("Key is already in the collection");
        }

        db.Entries.Add(entry);
        await db.SaveChangesAsync();
    }

    public DataBasedList(string connectionString)
    {
        this._connectionString = connectionString;
    }

    public void Add(TEntity entry)
    {
        var db = new DataBaseContext<TEntity>(_connectionString);
        if (Find(entry.Key, out var result))
        {
            throw new ArgumentException("Key is already in the collection");
        }

        db.Entries.Add(entry);
        db.SaveChanges();
    }

    public async Task<bool> AddExclusiveAsync(TEntity entry)
    {
        var db = new DataBaseContext<TEntity>(_connectionString);
        var wasElement = db.Entries.Any(x => x.Key.Equals(entry.Key));
        if (wasElement)
        {
            db.Entries.Remove(db.Entries.First(x => x.Key.Equals(entry.Key)));
            await db.SaveChangesAsync();
        }

        AddAsync(entry);
        return wasElement;
    }

    public bool AddExclusive(TEntity entry)
    {
        var db = new DataBaseContext<TEntity>(_connectionString);
        var wasElement = db.Entries.Any(x => x.Key.Equals(entry.Key));
        if (wasElement)
        {
            db.Entries.Remove(db.Entries.First(x => x.Key.Equals(entry.Key)));
            db.SaveChanges();
        }

        Add(entry);
        return wasElement;
    }

    public bool Find(TKey id, out TEntity? result)
    {
        var db = new DataBaseContext<TEntity>(_connectionString);
        result = db.Entries
                   .FirstOrDefault(x => x.Key.Equals(id));
        return result != null;
    }

    public IEnumerable<TEntity> Find(Func<TEntity, bool> predicate)
    {
        var db = new DataBaseContext<TEntity>(_connectionString);
        return db.Entries
                 .Where(predicate);
    }

    public void ForEachReadOnly(Action<TEntity> predicate)
    {
        var db = new DataBaseContext<TEntity>(_connectionString);
        foreach (var havingKey in db.Entries)
        {
            predicate(havingKey);
        }
    }

    public int Count()
    {
        var db = new DataBaseContext<TEntity>(_connectionString);
        return db.Entries.Count();
    }
}