namespace DataBasedCollection;

public class MemoryList<TEntity, TKey> : ISafeCollection<TEntity, TKey>
    where TEntity : HavingKey<TKey> where TKey : IComparable
{
    private List<TEntity> _list;

    public MemoryList(List<TEntity> list)
    {
        _list = list;
    }

    public void Remove(TEntity entry)
    {
        if (_list.Contains(entry))
            _list.Remove(entry);
    }

    public void RemoveAsync(TEntity entry)
    {
        Remove(entry);
    }

    public void AddAsync(TEntity entry)
    {
        Add(entry);
    }

    public void Add(TEntity entry)
    {
        if (_list.Count(x => x.Key.CompareTo(entry.Key) == 0) == 0)
            _list.Add(entry);
        else
        {
            throw new ArgumentException("Key collision");
        }
    }

    public async Task<bool> AddExclusiveAsync(TEntity entry)
    {
        return AddExclusive(entry);
    }

    public bool AddExclusive(TEntity entry)
    {
        var wasElement = _list.Any(x => x.Key.CompareTo(entry.Key) == 0);
        if (wasElement)
            _list.Remove(_list.First(x => x.Key.CompareTo(entry.Key) == 0));
        Add(entry);
        return wasElement;
    }

    public bool Find(TKey key, out TEntity? result)
    {
        result = _list.FirstOrDefault(x => x.Key.CompareTo(key) == 0);
        return result != null;
    }

    public IEnumerable<TEntity> Find(Func<TEntity, bool> predicate)
    {
        return _list.Where(predicate);
    }

    public void ForEachReadOnly(Action<TEntity> predicate)
    {
        _list.ForEach(predicate);
    }

    public int Count()
    {
        return _list.Count;
    }
}