﻿using Microsoft.EntityFrameworkCore;

namespace DataBasedCollection;

public class DataBaseContext<T> : DbContext where T : class
{
    public DbSet<T> Entries { get; set; }
    public string ConnectionString;

    public DataBaseContext(string connectionString)
    {
        ConnectionString = connectionString;
        Database.EnsureCreated();
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql(ConnectionString);
    }
}

