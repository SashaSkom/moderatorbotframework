using Microsoft.EntityFrameworkCore;

namespace DataBasedCollection;

public class CachedDatabasedCollection<T, TKey> : ISafeCollection<T, TKey>
    where T : HavingKey<TKey>
{
    private CachedRestrictedList<T> _cache;
    private CachedRestrictedList<TKey> _whiteCache;
    private readonly string _connectionString;
    private readonly int _cacheSize;


    public CachedDatabasedCollection(string connectionString, int cacheSize)
    {
        _connectionString = connectionString;
        _cacheSize = cacheSize;
        _cache = new CachedRestrictedList<T>(_cacheSize);
        _whiteCache = new CachedRestrictedList<TKey>(_cacheSize);
    }

    public void Remove(T entry)
    {
        _cache = new CachedRestrictedList<T>(_cacheSize);
        var db = new DataBaseContext<T>(_connectionString);
        db.Remove(entry);
        db.SaveChanges();
    }

    public async void RemoveAsync(T entry)
    {
        lock (_cache)
        {
            _cache = new CachedRestrictedList<T>(_cacheSize);
        }

        var db = new DataBaseContext<T>(_connectionString);
        db.Remove(entry);
        await db.SaveChangesAsync();
    }

    public async void AddAsync(T entry)
    {
        lock (_cache)
        {
            _whiteCache = new CachedRestrictedList<TKey>(_cacheSize);
        }

        var db = new DataBaseContext<T>(_connectionString);
        db.Entries.Add(entry);
        await db.SaveChangesAsync();
    }

    public void Add(T entry)
    {
        _whiteCache = new CachedRestrictedList<TKey>(_cacheSize);
        var db = new DataBaseContext<T>(_connectionString);
        db.Entries.Add(entry);
        db.SaveChanges();
    }

    public async Task<bool> AddExclusiveAsync(T entry)
    {
        _cache = new CachedRestrictedList<T>(_cacheSize);
        var db = new DataBaseContext<T>(_connectionString);
        var wasElement = db.Entries.Any(x => x.Key.Equals(entry.Key));
        if (wasElement)
        {
            db.Entries.Remove(db.Entries.First(x => x.Key.Equals(entry.Key)));
            await db.SaveChangesAsync();
        }

        AddAsync(entry);
        return wasElement;
    }

    public bool AddExclusive(T entry)
    {
        _cache = new CachedRestrictedList<T>(_cacheSize);
        var db = new DataBaseContext<T>(_connectionString);
        var wasElement = db.Entries.Any(x => x.Key.Equals(entry.Key));
        if (wasElement)
        {
            db.Entries.Remove(db.Entries.First(x => x.Key.Equals(entry.Key)));
            db.SaveChanges();
        }

        Add(entry);
        return wasElement;
    }

    public bool Find(TKey key, out T? result)
    {
        result = null;
        var elementsFromCache = _cache.CachedGetElementsOrNull(x => x.Key.Equals(key)).ToList();
        if (elementsFromCache.Count != 0)
        {
            result = elementsFromCache[0];
            return true;
        }

        var whiteCache = _whiteCache.CachedGetElementsOrNull(x => x.Equals(key)).ToList();
        if (whiteCache.Count != 0)
        {
            return false;
        }

        var db = new DataBaseContext<T>(_connectionString);

        foreach (var entry in db.Entries)
        {
            if (entry.Key.Equals(key))
            {
                result = entry;
                _cache.Add(entry);
                return true;
            }
        }

        _whiteCache.Add(key);
        return false;
    }

    public IEnumerable<T> Find(Func<T, bool> predicate)
    {
        var db = new DataBaseContext<T>(_connectionString);
        return db.Entries
                 .Where(predicate);
    }

    public void ForEachReadOnly(Action<T> predicate)
    {
        var db = new DataBaseContext<T>(_connectionString);
        foreach (var entry in db.Entries)
        {
            predicate(entry);
        }
    }

    public int Count()
    {
        var db = new DataBaseContext<T>(_connectionString);
        return db.Entries.Count();
    }
}