namespace DataBasedCollection;

public interface ISafeCollection<TEntity, TKey> where TEntity : HavingKey<TKey>
{
    public void Remove(TEntity entry);
    public void RemoveAsync(TEntity entry);
    public void AddAsync(TEntity entry);
    public void Add(TEntity entry);
    public Task<bool> AddExclusiveAsync(TEntity entry);
    public bool AddExclusive(TEntity entry);
    public bool Find(TKey key, out TEntity? result);
    public IEnumerable<TEntity> Find(Func<TEntity, bool> predicate);

    public void ForEachReadOnly(Action<TEntity> predicate);

    public int Count();
}