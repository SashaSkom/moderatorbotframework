﻿using ModeratorBotFrameworkCore.Bot;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace TelegramPlugin;

public class TelegramPlugin : IPlugin
{
    private readonly string _token;
    private readonly TelegramBotClient _bot;
    private long _chatId;
    private readonly Func<Message, bool> _messageFilter;
    
    public TelegramPlugin(string token, Func<Message, bool> messageFilter)
    {
        _token = token;
        _bot = new TelegramBotClient(_token);
        _messageFilter = messageFilter;
    }

    public override async Task AcceptAction(IActionDto action)
    {
        await action.Visit(this);
    }

    public override async Task DeleteMessage(DeleteMessageActionDto target)
    {
        try
        {
            await _bot.DeleteMessageAsync(_chatId, int.Parse(target.MessageId));
        }
        catch
        {
            // ignored
        }
    }

    public override async Task<MessageDTO?> WriteMessage(WriteMessageActionDto target)
    {
        return ConvertToMessageDto(await _bot.SendTextMessageAsync(target.MessageChatId, target.Text));
    }

    public override MessageDTO SendPicture(SendPictureActionDto target)
    {
        using var img = System.IO.File.Open(target.PathToImg, FileMode.Open);
        return ConvertToMessageDto(_bot.SendPhotoAsync(target.MessageChatId, img).Result);
    }

    public override MessageDTO SendPictureWithMessage(SendPictureWithMessageActionDto target)
    {
        using var img = System.IO.File.Open(target.PathToImg, FileMode.Open);
        return ConvertToMessageDto(_bot.SendPhotoAsync(target.MessageChatId, img, target.Text).Result);
    }

    private static MessageDTO? ConvertToMessageDto(Message? rawMessage)
    {
        if (rawMessage == null)
            return null;

        var messageDto = new MessageDTO()
        {
            Text = rawMessage.Text,
            MessageId = rawMessage.MessageId.ToString(),
            Member = new Member(rawMessage.From.Id, rawMessage.From.Username),
            ReplyToMessage = ConvertToMessageDto(rawMessage.ReplyToMessage),
            ChatId = rawMessage.Chat.Id
        };

        return messageDto;
    }

    public async Task DoWork()
    {
        await _bot.SetWebhookAsync("");
        var offset = 0;
        while (true)
        {
            var updates = await _bot.GetUpdatesAsync(offset);
            foreach (var update in updates)
            {
                var message = update.Message;
                if (message != null && _messageFilter(message))
                {
                    _chatId = message.Chat.Id;

                    var messageDto = ConvertToMessageDto(message);

                    foreach (var tasks in Observers.Select(observer => observer.Invoke(messageDto)))
                    {
                        await foreach (var task in tasks)
                        {
                            AcceptAction(task);
                        }
                    }
                }

                offset = update.Id + 1;
            }
        }
    }
}