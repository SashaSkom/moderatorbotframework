﻿using System.Drawing;
using ModeratorBotFrameworkCore.Bot;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;

namespace ConsoleBotPlugin;

public class ConsolePlugin : IPlugin
{
    public void DeleteMessage(DeleteMessageActionDto target)
    {
        Console.WriteLine($"I should delete message {target.MessageId}", Color.Red);
    }

    public override async Task<MessageDTO?> WriteMessage(WriteMessageActionDto target)
    {
        Console.WriteLine(target.Text);
        return null;
    }

    public async override Task AcceptAction(IActionDto action)
    {
        action.Visit(this);
    }

    public async Task DoWork()
    {
        while (1 == 1)
        {
            var rawMessage = Console.ReadLine();

            var message = new MessageDTO()
            {
                Text = rawMessage
            };

            foreach (var observer in Observers)
            {
                var actions = observer.Invoke(message);
                await foreach (var task in actions)
                {
                    AcceptAction(task);
                }
            }
        }
    }

    public void SendPicture(SendPictureActionDto target)
    {
        throw new NotImplementedException();
    }
}