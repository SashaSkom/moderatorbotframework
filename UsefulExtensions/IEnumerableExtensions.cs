namespace UsefulExtensions;

public static class EnumerableExtensions
{
    public static bool NotContains<T>(this IEnumerable<T> ienumerable, T element)
    {
        return !ienumerable.Contains(element);
    }
    
    public static bool NotContains<T>(this List<T> ienumerable, T element)
    {
        return !ienumerable.Contains(element);
    }
    
    public static bool NotContains<T>(this HashSet<T> ienumerable, T element)
    {
        return !ienumerable.Contains(element);
    }
    public static bool NotContainsKey<TKey, TValue>(this IDictionary<TKey, TValue> ienumerable, TKey element) where TKey : notnull
    {
        return !ienumerable.ContainsKey(element);
    }
}