﻿namespace UsefulExtensions;

public static class DateTimeExtensions
{
    public static DateTime OffsetInSeconds(this DateTime dateTime, int seconds)
    {
        return dateTime + new TimeSpan(0, 0, 0, seconds);
    }
    public static DateTime OffsetInDays(this DateTime dateTime, int days)
    {
        return dateTime + new TimeSpan(days, 0, 0, 0);
    }
}