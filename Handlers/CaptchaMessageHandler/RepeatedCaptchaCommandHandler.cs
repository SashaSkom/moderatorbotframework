﻿/*using Bot;
using Bot.Actions;
using Bot.DTO;
using MessagePipeline;
using MessagePipeline.IContextAction;
using ModeratorBotFramework.DTO;
using UsefulExtensions;

namespace CaptchaMessageHandler
{
    [ImplementingOptional("MembersToCaptcha")]
    [Required("MessageValidationInfo")]
    public class RepeatedCaptchaCommandHandle : IMessageHandler
    {
        public string CommandText;
        private readonly IMemberAction _removePenaltiesForNonAuthorizedMember;

        public RepeatedCaptchaCommandHandle(string commandText, IMemberAction removePenaltiesForNonAuthorizedMember)
        {
            CommandText = commandText;
            this._removePenaltiesForNonAuthorizedMember = removePenaltiesForNonAuthorizedMember;
        }

        public IEnumerable<IActionDto> HandleMessage(MessageDTO message, Dictionary<string, IContextEntry> context)
        {
            if (message.Text == null) yield break;
            var ms = message.Text.Split();

            if (ms[0] == CommandText && ms.Length == 3)
            {
                var privilegeInfo = (MessageValidationInfoContextEntry)context["MessageValidationInfo"];

                if (privilegeInfo.MessageSenderPrivilegeLevel < 1) yield break;

                long id = 0;
                try
                {
                    id = long.Parse(ms[1]);
                }
                catch (Exception)
                {

                    Console.WriteLine("Не правильный формат id");
                }

                var name = ms[3];

                if (context.NotContainsKey("MembersToCaptcha"))
                {
                    context["MembersToCaptcha"] = new CaptchaContextEntry();
                }

                var captchaContextEntry = (CaptchaContextEntry)context["MembersToCaptcha"];

                var user = new UserDto(new Member(id, name), false);
                if (captchaContextEntry.Values.NotContains(user))
                {
                    _removePenaltiesForNonAuthorizedMember.Realize(context, new List<Mem>() { user });
                    captchaContextEntry.AddValue(user);
                }
            }

            yield break;
        }
    }
}*/
