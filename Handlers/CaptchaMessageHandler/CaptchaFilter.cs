﻿using DataBasedCollection;
using Handlers.CaptchaMessageHandler.Captcha;
using Handlers.CaptchaMessageHandler.Configurators;
using Handlers.CaptchaMessageHandler.Session;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.IContextAction;

namespace Handlers.CaptchaMessageHandler
{
    [RequiredBotOptions("SendPicture", "WriteMessage", "DeleteMessage")]
    [RequiredOptional("MembersToCaptcha")]
    public class CaptchaFilter : AuthorizationFilterBase
    {
        private readonly ISafeCollection<MemberCaptchaStatusDto, long> _users;
        private int AttemptsNumber => _configurator.AttemptsNumber;
        private readonly IMemberAction _penaltiesForNonAuthorizedMember;
        private int SessionLifeTimeInSeconds => _configurator.LifeTimeInSeconds;
        private bool _coroutineStarted;
        private int CaptchaLength => _configurator.CaptchaLength;
        private readonly ICaptchaHandlerConfigurator _configurator;

        public CaptchaFilter(ISafeCollection<MemberCaptchaStatusDto, long> users, 
                             IMemberAction penaltiesForNonAuthorizedMember,
                             ICaptchaHandlerConfigurator configurator)
        {
            _users = users;
            _penaltiesForNonAuthorizedMember = penaltiesForNonAuthorizedMember;
            _configurator = configurator;
        }

        public async override IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO message,
                                                              Dictionary<string, IContextEntry> context)
        {
            lock (sessions)
            {
                if (context.ContainsKey("MembersToCaptcha"))
                {
                    ((CaptchaContextEntry) context["MembersToCaptcha"]).Values
                                                                       .ForEach(user =>
                                                                       {
                                                                           _users.Remove(user);
                                                                           var newSession = new CaptchaSession(
                                                                               AttemptsNumber,
                                                                               CaptchaGenerator.CreateCaptcha(
                                                                                   CaptchaLength),
                                                                               SessionLifeTimeInSeconds, user, _configurator);
                                                                           sessions.Add(newSession);
                                                                       });
                }

                if (!_coroutineStarted)
                {
                    _coroutineStarted = true;
                    StartCoroutine(SessionCleanerCoroutine);
                }

                _users.Find(message.Member.Id, out var user);
                if (user is not null) yield break;

                var newSession = sessions
                    .FirstOrDefault(x => x.MemberCaptchaStatus.MemberId == message.Member.Id);

                if (newSession == null)
                {
                    newSession = new CaptchaSession(AttemptsNumber, CaptchaGenerator.CreateCaptcha(CaptchaLength),
                        SessionLifeTimeInSeconds,
                        new MemberCaptchaStatusDto(message.Member.Id, false), _configurator);
                    sessions.Add(newSession);
                }

                var acts = sessions.Where(x => x.MemberCaptchaStatus.MemberId == message.Member.Id)
                                   .SelectMany(x => x.HandleMessageByMember(message)).ToList();

                foreach (var session in sessions.Where(x => x.Condition == SessionBase.SessionStatus.Passed)
                                                .ToList())
                {
                    _users.Add(new MemberCaptchaStatusDto(message.Member.Id, true));
                    session.DeleteSession();
                    sessions.Remove(session);
                }

                foreach (var session in sessions.Where(x => x.Condition == SessionBase.SessionStatus.NotPassed)
                                                .ToList())
                {
                    _users.Add(new MemberCaptchaStatusDto(message.Member.Id, false));
                    _penaltiesForNonAuthorizedMember.Realize(context,
                        new List<Member> {new Member(session.MemberCaptchaStatus.MemberId, "")});
                    session.DeleteSession();
                    sessions.Remove(session);
                }

                foreach (var actionDto in acts)
                {
                    yield return actionDto;
                }
            }
        }
    }
}