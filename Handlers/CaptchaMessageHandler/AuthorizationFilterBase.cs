﻿using Handlers.CaptchaMessageHandler.Session;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;
using UsefulExtensions;

namespace Handlers.CaptchaMessageHandler
{
    public abstract class AuthorizationFilterBase : CoroutinedMessageHandler
    {
        protected readonly List<CaptchaSession> sessions;

        public AuthorizationFilterBase()
        {
            sessions = new List<CaptchaSession>(); 
        }

        protected IEnumerable<WaitForSeconds> SessionCleanerCoroutine()
        {
            while (true)
            {
                yield return new WaitForSeconds(60);
                lock (sessions)
                {
                    sessions.Where(x => x.LastUpdate.OffsetInSeconds(x.LifeTimeInSeconds) < DateTime.UtcNow).ToList()
                            .ForEach(x =>
                            {
                                x.Condition = CaptchaSession.SessionStatus.NotPassed;
                                x.DeleteSession();
                            });
                }

                yield return new WaitForSeconds(60);
            }
        }
    }
}
