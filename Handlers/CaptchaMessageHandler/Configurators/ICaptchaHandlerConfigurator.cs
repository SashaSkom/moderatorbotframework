﻿namespace Handlers.CaptchaMessageHandler.Configurators;
public interface ICaptchaHandlerConfigurator
{
    public Func<string, string> Hello { get; }
    public Func<int, string> AnswerEntered { get; }
    public Func<int, string> Wrong { get; }
    
    public int AttemptsNumber { get; }
    public int CaptchaLength { get; }
    
    public int LifeTimeInSeconds { get; }
}
