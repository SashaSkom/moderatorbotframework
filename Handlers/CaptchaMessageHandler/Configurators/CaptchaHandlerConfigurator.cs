﻿namespace Handlers.CaptchaMessageHandler.Configurators;
public class CaptchaHandlerConfigurator : ICaptchaHandlerConfigurator
{
    public CaptchaHandlerConfigurator(Func<string, string> hello, Func<int, string> enterKaptcha, Func<int, string> wrong, int attemptsNumber, int captchaLength, int lifeTimeInSeconds)
    {
        Hello = hello;
        AnswerEntered = enterKaptcha;
        Wrong = wrong;
        AttemptsNumber = attemptsNumber;
        CaptchaLength = captchaLength;
        LifeTimeInSeconds = lifeTimeInSeconds;
    }

    public Func<string, string> Hello { get; }
    public Func<int, string> AnswerEntered { get; }
    public Func<int, string> Wrong { get; }
    public int AttemptsNumber { get; }
    public int CaptchaLength { get; }
    public int LifeTimeInSeconds { get; }
}
