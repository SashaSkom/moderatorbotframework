﻿namespace Handlers.CaptchaMessageHandler.Configurators;

public class CaptchaHandlerRandomizedConfigurator : ICaptchaHandlerConfigurator
{
    private readonly List<Func<int, string>> _enterKaptchaMessages;
    private readonly List<Func<string,string>> _helloMessages;
    private readonly List<Func<int,string>> _wrongMessages;
    private readonly Random _rnd;

    public CaptchaHandlerRandomizedConfigurator(List<Func<int, string>> enterKaptchaMessages, List<Func<string, string>> helloMessages,
                                    List<Func<int, string>> wrongMessages, int attemptsNumber, int captchaLength, int lifeTimeInSeconds)
    {
        _helloMessages = helloMessages;
        _enterKaptchaMessages = enterKaptchaMessages;
        _wrongMessages = wrongMessages;
        AttemptsNumber = attemptsNumber;
        CaptchaLength = captchaLength;
        LifeTimeInSeconds = lifeTimeInSeconds;
        _rnd = new Random();
    }

    public Func<string, string> Hello => _helloMessages[_rnd.Next(_helloMessages.Count)];
    public Func<int, string> AnswerEntered => _enterKaptchaMessages[_rnd.Next(_enterKaptchaMessages.Count)];
    public Func<int, string> Wrong => _wrongMessages[_rnd.Next(_wrongMessages.Count)];
    public int AttemptsNumber { get; }
    public int CaptchaLength { get; }
    public int LifeTimeInSeconds { get; }
}