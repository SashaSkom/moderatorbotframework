﻿using ModeratorBotFrameworkCore.MessagePipeline;

namespace Handlers.CaptchaMessageHandler
{
    public class CaptchaContextEntry : ContextEntry<MemberCaptchaStatusDto>
    {
        public List<MemberCaptchaStatusDto> Values;

        public void AddValue(MemberCaptchaStatusDto value)
        {
            Values.Add(value);
        }

        public CaptchaContextEntry()
        {
            Values = new List<MemberCaptchaStatusDto>();
        }
    }
}
