﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataBasedCollection;

namespace Handlers.CaptchaMessageHandler
{
    [Table("CaptchaInformation")]
    public class MemberCaptchaStatusDto : HavingKey<long>
    {
        public bool IsAuthorized { get; private set; }
        [NotMapped] public long MemberId => Key;
        [Key]
        [Column("MemberId")]
        public override long Key { get; set; }

        public MemberCaptchaStatusDto(long key, bool isAuthorized)
        {
            IsAuthorized = isAuthorized;
            Key = key;
        }

        public override bool Equals(object? obj)
        {
            if (obj is MemberCaptchaStatusDto user)
            {
                return MemberId == user.MemberId;
            }
            return false;
        }
    }
}
