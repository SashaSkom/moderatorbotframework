﻿
namespace Handlers.CaptchaMessageHandler.Captcha
{
    public class Captcha
    {
        public string Code;
        public string Path;

        public Captcha(string code, string path)
        {
            Code = code;
            Path = path;
        }
    }
}
