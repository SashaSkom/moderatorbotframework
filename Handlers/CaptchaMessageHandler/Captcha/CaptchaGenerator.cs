﻿using System.Drawing;
using System.Drawing.Imaging;
using System.Net.Mime;
using CaptchaGen.NetCore;

namespace Handlers.CaptchaMessageHandler.Captcha
{
    public class CaptchaGenerator
    {
        private static Bitmap CreateImage(int Width, int Height, string text)
        {
            Random rnd = new Random();

            //Создадим изображение
            Bitmap result = new Bitmap(Width, Height);

            //Вычислим позицию текста
            int Xpos = rnd.Next(0, Width - 50);
            int Ypos = rnd.Next(15, Height - 15);

            //Добавим различные цвета
            Brush[] colors =
            {
                Brushes.Black,
                Brushes.Red,
                Brushes.RoyalBlue,
                Brushes.Green
            };

            //Укажем где рисовать
            Graphics g = Graphics.FromImage(result); //ToDo cast

            //Пусть фон картинки будет серым
            g.Clear(Color.Gray);

            //Сгенерируем текст

            //Нарисуем сгенирируемый текст
            g.DrawString(text,
                new Font("Arial", 15),
                colors[rnd.Next(colors.Length)],
                new PointF(Xpos, Ypos));

            //Добавим немного помех
            /////Линии из углов
            g.DrawLine(Pens.Black,
                new Point(0, 0),
                new Point(Width - 1, Height - 1));
            g.DrawLine(Pens.Black,
                new Point(0, Height - 1),
                new Point(Width - 1, 0));
            ////Белые точки
            for (int i = 0; i < Width; ++i)
                for (int j = 0; j < Height; ++j)
                    if (rnd.Next() % 20 == 0)
                        result.SetPixel(i, j, Color.White);

            return result;
        }

        public static Captcha CreateCaptcha(int lengthCaptcha, bool useCaptchaGen = false)
        {
            string code = ImageFactory.CreateCode(lengthCaptcha);
            var path = $"{Directory.GetCurrentDirectory()}\\captcha.jpg";

            if (useCaptchaGen)
            {
                using FileStream fs = File.OpenWrite(path);
                using Stream picStream = ImageFactory.BuildImage(code, 200, 200, 40, 10, ImageFormat.Jpeg);
                picStream.CopyTo(fs);
            }
            else
            {
                var image = CreateImage(200, 200, code);
                image.Save(path, ImageFormat.Jpeg);
            }
            
            return new Captcha(code, path);
        }
    }
}
