﻿using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;

namespace Handlers.CaptchaMessageHandler.Session
{
    public abstract class SessionBase
    {
        public List<InvocableAction> SessionActions;

        public SessionBase()
        {
            SessionActions = new List<InvocableAction>();
        }

        public abstract IEnumerable<IActionDto> SendAuthorizationRequest(MessageDTO message);

        public abstract IEnumerable<IActionDto> HandleMessageByMember(MessageDTO messageDto);

        public void DeleteSession()
        {
            foreach (var invocableAction in SessionActions)
            {
                invocableAction.InitiateActionAsync();
            }
        }
        public enum SessionStatus
        {
            Starting,
            InProcess,
            Passed,
            NotPassed
        }
    }
}
