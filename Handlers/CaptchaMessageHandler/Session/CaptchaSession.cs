﻿using Handlers.CaptchaMessageHandler.Configurators;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;

namespace Handlers.CaptchaMessageHandler.Session
{
    public class CaptchaSession : SessionBase
    {
        public Captcha.Captcha Captcha;
        public int AttemptsNumber;
        public MemberCaptchaStatusDto MemberCaptchaStatus;
        public DateTime LastUpdate;
        public readonly int LifeTimeInSeconds;
        public SessionStatus Condition = SessionStatus.Starting;
        ICaptchaHandlerConfigurator _handlerConfigurator;

        public CaptchaSession(int numberAttempts, Captcha.Captcha captcha, int lifeTimeInSeconds, MemberCaptchaStatusDto member, ICaptchaHandlerConfigurator handlerConfigurator)
        {
            AttemptsNumber = numberAttempts;
            Captcha = captcha;
            LifeTimeInSeconds = lifeTimeInSeconds;
            MemberCaptchaStatus = member;
            _handlerConfigurator = handlerConfigurator;
        }

        public override IEnumerable<IActionDto> SendAuthorizationRequest(MessageDTO message)
        {
            var action = new SendPictureWithMessageWithOnDemandDelete(Captcha.Path,
                _handlerConfigurator.Hello(message.Member.Name),
                message.ChatId);
            SessionActions.Add(action);
            yield return action;
        }

        private IEnumerable<IActionDto> WriteMessage(MessageDTO message)
        {
            var action = new WriteMessageWithOnDemandDelete(_handlerConfigurator.Wrong(AttemptsNumber), message.ChatId);
            SessionActions.Add(action);
            yield return action;
        }

        public override IEnumerable<IActionDto> HandleMessageByMember(MessageDTO message)
        {
            LastUpdate = DateTime.UtcNow;
            var messDeleter =
                new OnInvocationActionWrapper<DeleteMessageActionDto>(new DeleteMessageActionDto(message.MessageId));
            SessionActions.Add(messDeleter);
            if (Condition == SessionStatus.Starting)
            {
                Condition = SessionStatus.InProcess;
                return SendAuthorizationRequest(message).Concat(new[] {messDeleter});
            }

            if (Condition == SessionStatus.InProcess)
            {
                AttemptsNumber -= 1;
                if (message.Text == Captcha.Code)
                {
                    Condition = SessionStatus.Passed;
                    return (new[] {messDeleter});
                }

                if (AttemptsNumber == 0)
                {
                    Condition = SessionStatus.NotPassed;
                    //_punishment.Realize(context, new[] {User});
                    return (new[] {messDeleter});
                }

                return WriteMessage(message).Concat(new[] {messDeleter});
            }

            return (new[] {messDeleter});
        }
    }
}