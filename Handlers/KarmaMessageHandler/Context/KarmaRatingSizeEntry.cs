﻿using ModeratorBotFrameworkCore.MessagePipeline;

namespace Handlers.KarmaMessageHandler.Context
{
    public class KarmaRatingSizeEntry : ContextEntry<int>
    {
        public int RatingSize;

        public void AddRatingSize(int ratingSize)
        {
            RatingSize = ratingSize;
        }
    }
}
