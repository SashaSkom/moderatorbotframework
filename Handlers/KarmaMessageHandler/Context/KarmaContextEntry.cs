﻿using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;

namespace Handlers.KarmaMessageHandler.Context
{
    public class KarmaContextEntry : ContextEntry<string>
    {
        public List<Member> Values;

        public void AddValue(Member value)
        {
            Values.Add(value);
        }

        public KarmaContextEntry()
        {
            Values = new List<Member>();
        }
    }
}
