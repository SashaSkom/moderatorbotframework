﻿using Handlers.DefaultValidator;
using Handlers.KarmaMessageHandler.Configurators;
using Handlers.KarmaMessageHandler.Context;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;
using UsefulExtensions;

namespace Handlers.KarmaMessageHandler.Commands;

[ImplementingOptional("MembersNameKeyWithKarma")]
[Required("MessageValidationInfo")]
public class GetKarmaCommand : IMessageHandler
{
    private const string MembersNameKeyWithKarma = "MembersNameKeyWithKarma";
    private const string MessageValidationInfo = "MessageValidationInfo";
    private string Command => _handlerConfigurator.GetCarmaCommand;
    private readonly IKarmaHandlerConfigurator _handlerConfigurator;

    public GetKarmaCommand(IKarmaHandlerConfigurator handlerConfigurator)
    {
        _handlerConfigurator = handlerConfigurator;
    }

    public async IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO message, Dictionary<string, IContextEntry> context)
    {
        if (message.Text == null) yield break;

        var splitMessageText = message.Text.Split();
        if (splitMessageText[0] != Command) yield break;

        if (splitMessageText.Length > 1)
        {
            yield return new WriteMessageActionDto(_handlerConfigurator.WrongFormat, message.ChatId);
            yield break;
        }

        Member member;
        if (message.ReplyToMessage is {Member: { }})
        {
            member = message.ReplyToMessage.Member;
            var privilegeInfo = (MessageValidationInfoContextEntry) context[MessageValidationInfo];
            if (privilegeInfo.MessageSenderPrivilegeLevel < 1) yield break;
        }
        else
        {
            member = message.Member;
        }


        if (context.NotContainsKey(MembersNameKeyWithKarma))
            context[MembersNameKeyWithKarma] = new KarmaContextEntry();

        var karmaContextEntry = (KarmaContextEntry) context[MembersNameKeyWithKarma];

        if (karmaContextEntry.Values.NotContains(member))
            karmaContextEntry.AddValue(member);
    }
}