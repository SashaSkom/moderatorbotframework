﻿using Handlers.KarmaMessageHandler.Configurators;
using Handlers.KarmaMessageHandler.Context;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;
using UsefulExtensions;

namespace Handlers.KarmaMessageHandler.Commands
{
    [ImplementingOptional("KarmaRatingSize")]
    public class GetKarmaRatingCommand : IMessageHandler
    {
        private const string KarmaRatingSize = "KarmaRatingSize";
        private string Command => _configurator.GetCarmaRatingCommand;
        private int RatingSize => _configurator.RatingSize;
        private readonly IKarmaHandlerConfigurator _configurator;

        public GetKarmaRatingCommand(IKarmaHandlerConfigurator configurator)
        {
            _configurator = configurator;
        }

        public async IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO message, Dictionary<string, IContextEntry> context)
        {
            if (message.Text != Command)
                yield break;

            if (context.NotContainsKey(KarmaRatingSize))
                context[KarmaRatingSize] = new KarmaRatingSizeEntry();

            var karnaRatingSizeContext = (KarmaRatingSizeEntry)context[KarmaRatingSize];

            karnaRatingSizeContext.AddRatingSize(RatingSize);
        }
    }
}
