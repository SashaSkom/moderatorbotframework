using Handlers.KarmaMessageHandler.Data;

namespace Handlers.KarmaMessageHandler.Configurators;

public abstract class KarmaHandlerConfiguratorBase : IKarmaHandlerConfigurator
{
    protected KarmaHandlerConfiguratorBase(string getCarmaCommand, string getCarmaRatingCommand, int ratingSize, int karmaReward)
    {
        GetCarmaCommand = getCarmaCommand;
        GetCarmaRatingCommand = getCarmaRatingCommand;
        RatingSize = ratingSize;
        KarmaReward = karmaReward;
    }

    public abstract string WrongFormat { get; }
    public abstract string UserDoesNotExist { get; }
    public abstract string LackOfKarma { get; }
    public abstract Func<MemberKarmaDto, string> KarmaLevel { get; }
    public abstract Func<string, int, string> GetKarma { get; }
    public virtual string GetCarmaCommand { get; }
    public virtual string GetCarmaRatingCommand { get; }
    public int RatingSize { get; }
    public int KarmaReward { get; }
}