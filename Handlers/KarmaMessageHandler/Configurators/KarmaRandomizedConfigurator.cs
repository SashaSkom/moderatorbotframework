﻿using Handlers.KarmaMessageHandler.Data;

namespace Handlers.KarmaMessageHandler.Configurators;

public class KarmaRandomizedConfigurator : KarmaHandlerConfiguratorBase
{
    public override string WrongFormat => _wrongFormatMessages[_rnd.Next(_wrongFormatMessages.Count)];
    public override string LackOfKarma => _lackOfKarmaMessages[_rnd.Next(_lackOfKarmaMessages.Count)];
    public override string UserDoesNotExist => _userDoesNotExistMessages[_rnd.Next(_userDoesNotExistMessages.Count)];
    public override Func<MemberKarmaDto, string> KarmaLevel => _karmaLevelMessages[_rnd.Next(_karmaLevelMessages.Count)];
    public override Func<string, int, string> GetKarma => _getKarmaMessages[_rnd.Next(_getKarmaMessages.Count)];

    private readonly List<string> _wrongFormatMessages;
    private readonly List<string> _userDoesNotExistMessages;
    private readonly List<string> _lackOfKarmaMessages;
    private readonly List<Func<MemberKarmaDto, string>> _karmaLevelMessages;
    private readonly List<Func<string, int, string>> _getKarmaMessages;
    private readonly Random _rnd;

    public KarmaRandomizedConfigurator(List<string> wrongFormatMessages,
                                       List<string> userDoesNotExistMessages,
                                       List<string> lackOfKarmaMessages,
                                       List<Func<MemberKarmaDto, string>> karmaLevelMessages,
                                       List<Func<string, int, string>> getKarmaMessages,
                                       string getCarmaCommand, 
                                       string getCarmaRatingCommand,
                                       int ratingSize,
                                       int reward) 
        : base(getCarmaCommand, getCarmaRatingCommand, ratingSize, reward)
    {
        _wrongFormatMessages = wrongFormatMessages;
        _userDoesNotExistMessages = userDoesNotExistMessages;
        _lackOfKarmaMessages = lackOfKarmaMessages;
        _karmaLevelMessages = karmaLevelMessages;
        _getKarmaMessages = getKarmaMessages;
        _rnd = new Random();
    }
}