﻿using Handlers.KarmaMessageHandler.Data;

namespace Handlers.KarmaMessageHandler.Configurators;

public interface IKarmaHandlerConfigurator
{
    public string WrongFormat { get; }
    public string UserDoesNotExist { get; }
    public string LackOfKarma { get; }
    public Func<MemberKarmaDto, string> KarmaLevel { get; }
    public Func<string, int, string> GetKarma { get; }

    public string GetCarmaCommand { get; }
    
    public string GetCarmaRatingCommand { get; }
    
    public int RatingSize { get; }
    
    public int KarmaReward { get; }
}