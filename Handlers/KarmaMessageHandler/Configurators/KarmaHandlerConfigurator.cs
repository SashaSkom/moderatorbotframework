﻿using Handlers.KarmaMessageHandler.Data;

namespace Handlers.KarmaMessageHandler.Configurators;

public class KarmaHandlerConfigurator : KarmaHandlerConfiguratorBase
{
    public KarmaHandlerConfigurator(string wrongFormat, 
                            string userDoesNotExist, 
                            string lackOfKarma,
                            Func<MemberKarmaDto, string> karmaLevel, 
                            Func<string, int, string> getKarma,
                            string getCarmaCommand,
                            string getCarmaRatingCommand,
                            int ratingSize,
                            int reward) : base(getCarmaCommand, getCarmaRatingCommand, ratingSize, reward)
    {
        WrongFormat = wrongFormat;
        UserDoesNotExist = userDoesNotExist;
        LackOfKarma = lackOfKarma;
        KarmaLevel = karmaLevel;
        GetKarma = getKarma;
    }

    public override string WrongFormat { get; }
    public override string UserDoesNotExist { get; }
    public override string LackOfKarma { get; }
    public override Func<MemberKarmaDto, string> KarmaLevel { get; }
    public override Func<string, int, string> GetKarma { get; }
}