﻿using System.Text;
using DataBasedCollection;
using Handlers.KarmaMessageHandler.Configurators;
using Handlers.KarmaMessageHandler.Context;
using Handlers.KarmaMessageHandler.Data;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;

namespace Handlers.KarmaMessageHandler
{
    [RequiredOptional("MembersNameKeyWithKarma", "KarmaRatingSize")]
    public class KarmaHandler : IMessageHandler
    {
        private const string MembersNameKeyWithKarma = "MembersNameKeyWithKarma";
        private const string KarmaRatingSize = "KarmaRatingSize";

        private readonly List<string> _triggerWordsList;
        private int Reward => _handlerConfigurator.KarmaReward;
        private readonly IKarmaHandlerConfigurator _handlerConfigurator;


        private readonly ISafeCollection<MemberKarmaDto, long> _membersKarmaList;
        private Member? _memberWithLastMessage;

        public KarmaHandler(ISafeCollection<MemberKarmaDto, long> membersKarmaList,
                            List<string> triggerWordsList,
                            IKarmaHandlerConfigurator handlerConfigurator)
        {
            _membersKarmaList = membersKarmaList;
            _triggerWordsList = triggerWordsList;
            _handlerConfigurator = handlerConfigurator;
        }

        private IEnumerable<IActionDto> HandleGetKarmaCommand(MessageDTO message,
                                                              Dictionary<string, IContextEntry> context)
        {
            if (!context.ContainsKey(MembersNameKeyWithKarma))
                yield break;

            var requestedMembers = ((KarmaContextEntry) context[MembersNameKeyWithKarma]).Values;

            foreach (var member in requestedMembers)
            {
                if (!_membersKarmaList.Find(member.Id, out var memberKarmaEntry))
                {
                    var newEntry = new MemberKarmaDto(member, 0);
                    _membersKarmaList.Add(newEntry);
                    yield return new WriteMessageActionDto(_handlerConfigurator.KarmaLevel(newEntry), message.ChatId);
                    yield break;
                }

                yield return new WriteMessageActionDto(_handlerConfigurator.KarmaLevel(memberKarmaEntry!),
                    message.ChatId);
            }
        }

        private IEnumerable<IActionDto> HandleGetKarmaRatingCommand(MessageDTO message,
                                                                    Dictionary<string, IContextEntry> context)
        {
            if (!context.ContainsKey(KarmaRatingSize)) yield break;

            var karmaRatingSizeContext = (KarmaRatingSizeEntry) context[KarmaRatingSize];
            var ratingSize = karmaRatingSizeContext.RatingSize;
            var membersWithMaxKarma = GetMembersForRating(ratingSize);
            if (!membersWithMaxKarma.Any())
            {
                yield return new WriteMessageActionDto(_handlerConfigurator.LackOfKarma, message.ChatId);
                yield break;
            }

            var outputMessage = new StringBuilder();
            foreach (var member in membersWithMaxKarma)
                outputMessage.AppendLine(_handlerConfigurator.KarmaLevel(member));

            yield return new WriteMessageActionDto(outputMessage.ToString(), message.ChatId);
        }

        private MemberKarmaDto[] GetMembersForRating(int ratingSize)
        {
            var size = Math.Min(_membersKarmaList.Count(), ratingSize);
            var membersWithMaxKarma = new MemberKarmaDto[size];

            _membersKarmaList.ForEachReadOnly(member =>
            {
                for (int i = 0; i < size; i++)
                {
                    if (membersWithMaxKarma[i] is null)
                    {
                        membersWithMaxKarma[i] = member;
                        break;
                    }

                    if (member.Karma <= membersWithMaxKarma[i].Karma)
                        continue;

                    var lost = membersWithMaxKarma[i];
                    membersWithMaxKarma[i] = member;

                    if (i == (size - 1))
                        break;

                    for (var j = i + 1; j < size; j++)
                    {
                        (membersWithMaxKarma[j], lost) = (lost, membersWithMaxKarma[j]);
                    }

                    break;
                }
            });

            return membersWithMaxKarma;
        }

        private IEnumerable<IActionDto> HandleContext(MessageDTO message, Dictionary<string, IContextEntry> context)
        {
            return HandleGetKarmaRatingCommand(message, context)
                .Concat(HandleGetKarmaCommand(message, context));
        }

        public async IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO message, Dictionary<string, IContextEntry> context)
        {
            if (!_membersKarmaList.Find(message.Member.Id, out _))
            {
                _membersKarmaList.AddAsync(new MemberKarmaDto(message.Member, 0));
            }

            foreach (var action in HandleContext(message, context))
                yield return action;

            if (message.ReplyToMessage is null && _memberWithLastMessage == default
                || message.Text is null
                || !_triggerWordsList.Contains(message.Text.ToLower()))
            {
                _memberWithLastMessage = message.Member;
                yield break;
            }

            Member memberForKarma;

            if (message.ReplyToMessage is not null
                && message.Member != message.ReplyToMessage.Member)
            {
                memberForKarma = message.ReplyToMessage.Member;
            }
            else if (_memberWithLastMessage != null
                     && message.Member != _memberWithLastMessage)
            {
                memberForKarma = _memberWithLastMessage;
            }
            else
            {
                yield break;
            }

            if (memberForKarma is null)
                yield break;

            _membersKarmaList.Find(memberForKarma.Id, out var member);

            if (member is not null)
            {
                _membersKarmaList.AddExclusiveAsync(new MemberKarmaDto(member.MemberId, member.Name, member.Karma + Reward));
            }

            yield return new WriteMessageActionDto(
                _handlerConfigurator.GetKarma(memberForKarma.Name, Reward),
                message.ChatId);
            
            _memberWithLastMessage = message.Member;
        }
    }
}