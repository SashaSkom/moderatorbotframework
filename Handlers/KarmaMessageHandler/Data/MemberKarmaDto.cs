﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataBasedCollection;
using ModeratorBotFrameworkCore.Bot.DTO;

namespace Handlers.KarmaMessageHandler.Data
{
    [Table("Karma")]
    public sealed class MemberKarmaDto : HavingKey<long>
    {
        [Key] [Column("MemberId")]public override long Key { get; set; }
        
        public string Name { get; private set; }
        public int Karma { get; private set; }

        [NotMapped] public long MemberId => Key;

        public MemberKarmaDto(long key, string name, int karma)
        {
            Name = name;
            Karma = karma;
            Key = key;
        }

        public MemberKarmaDto(Member member, int karma)
        {
            Karma = karma;
            Name = member.Name;
            Key = member.Key;
        }
    }
}