using Handlers.DefaultValidator;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;

namespace Handlers.AmiliveCommandHandler;

[Required("MessageValidationInfo")]
public class AmILiveCommandHandler : IMessageHandler
{
    private const string MessageValidationInfo = "MessageValidationInfo";
    private readonly string _commandText;
    private readonly string _answerText;

    public AmILiveCommandHandler(string commandText, string answerText)
    {
        _commandText = commandText;
        _answerText = answerText;
    }

    public async IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO message, Dictionary<string, IContextEntry> context)
    {
        var privilege = ((MessageValidationInfoContextEntry) context[MessageValidationInfo])
            .MessageSenderPrivilegeLevel;
        if (message.Text?.Split()[0] != _commandText)
            yield break;

        if (privilege < 2)
            yield break;

        yield return new WriteMessageActionDto(_answerText, message.ChatId);
    }
}