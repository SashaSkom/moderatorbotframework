﻿namespace Handlers.DefaultValidator.Configurators;
public class PrivilegeBotRandomMessages : IValidatorConfigurator
{
    public Func<string, string, string> PrivilegeLevel => _privilegeLevelMessages[_rnd.Next(_privilegeLevelMessages.Count)];
    private readonly List<Func<string,string, string>> _privilegeLevelMessages;
    
    public string SetPrivilegeLevelCommandText { get; }
    public string GetPrivilegeLevelCommandText { get; }
    public string InvalidArgument { get; }

    private readonly Random _rnd;
    public PrivilegeBotRandomMessages(List<Func<string, string, string>> privilegeLevelMessages,
                                      string setPrivilegeLevelCommandText,
                                      string getPrivilegeLevelCommandText, string invalidArgument)
	{
        _privilegeLevelMessages = privilegeLevelMessages;
        SetPrivilegeLevelCommandText = setPrivilegeLevelCommandText;
        GetPrivilegeLevelCommandText = getPrivilegeLevelCommandText;
        InvalidArgument = invalidArgument;
        _rnd = new Random();
    }
}
