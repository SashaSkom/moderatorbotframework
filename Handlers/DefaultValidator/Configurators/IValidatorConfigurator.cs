﻿namespace Handlers.DefaultValidator.Configurators;

public interface IValidatorConfigurator
{
    public Func<string, string, string> PrivilegeLevel { get; }
    public string SetPrivilegeLevelCommandText { get; }
    public string GetPrivilegeLevelCommandText { get; }

    public string InvalidArgument { get; }
}