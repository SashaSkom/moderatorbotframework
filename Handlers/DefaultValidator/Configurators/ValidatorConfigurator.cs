﻿namespace Handlers.DefaultValidator.Configurators;
public class ValidatorConfigurator : IValidatorConfigurator
{
    public ValidatorConfigurator(Func<string, string, string> privilegeLevel,
                                 string setPrivilegeLevelCommandText,
                                 string getPrivilegeLevelCommandText, string invalidArgument)
    {
        PrivilegeLevel = privilegeLevel;
        SetPrivilegeLevelCommandText = setPrivilegeLevelCommandText;
        GetPrivilegeLevelCommandText = getPrivilegeLevelCommandText;
        InvalidArgument = invalidArgument;
    }

    public Func<string, string, string> PrivilegeLevel { get; }
    public string SetPrivilegeLevelCommandText { get; }
    public string GetPrivilegeLevelCommandText { get; }
    public string InvalidArgument { get; }
}
