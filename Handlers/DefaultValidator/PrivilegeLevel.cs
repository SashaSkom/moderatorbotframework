namespace Handlers.DefaultValidator;

public enum PrivilegeLevel
{
    Member,
    Moderator,
    Administrator
}