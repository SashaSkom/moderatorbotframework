using ModeratorBotFrameworkCore.MessagePipeline;

namespace Handlers.DefaultValidator;

public class MessageValidationInfoContextEntry : IContextEntry
{
    public int MessageSenderPrivilegeLevel;
    public int MessageVictimPrivilegeLevel;
    public string MessageSenderPrivilegeLevelName;
    public string MessageVictimPrivilegeLevelName;
    
}