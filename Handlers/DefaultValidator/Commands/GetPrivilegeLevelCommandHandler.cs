using Handlers.DefaultValidator.Configurators;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;

namespace Handlers.DefaultValidator.Commands;

[Required("MessageValidationInfo")]
public class GetPrivilegeLevelCommandHandler : IMessageHandler
{
    private const string MessageValidationInfo = "MessageValidationInfo";
    private string CommandText => _botMessages.GetPrivilegeLevelCommandText;
    private readonly IValidatorConfigurator _botMessages;

    public GetPrivilegeLevelCommandHandler(IValidatorConfigurator botMessages)
    {
        _botMessages = botMessages;
    }


    public async IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO message, Dictionary<string, IContextEntry> context)
    {
        if (message.Text == null || message.Text.Split()[0] != CommandText)
            yield break;

        var target = message.Member;
        var privilege = ((MessageValidationInfoContextEntry)context[MessageValidationInfo]).MessageSenderPrivilegeLevelName;

        if (message.ReplyToMessage != null)
        {
            target = message.ReplyToMessage.Member;
            privilege = ((MessageValidationInfoContextEntry)context[MessageValidationInfo]).MessageVictimPrivilegeLevelName;
        }

        yield return new WriteMessageActionDto(_botMessages.PrivilegeLevel(target.Name, privilege), message.ChatId);
    }
}