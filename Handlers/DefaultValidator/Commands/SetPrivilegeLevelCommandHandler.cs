using Handlers.DefaultValidator.Configurators;
using Handlers.DefaultValidator.ContextEntries;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;
using UsefulExtensions;

namespace Handlers.DefaultValidator.Commands;
[ImplementingOptional("SetPrivilege")]
public class SetPrivilegeLevelCommandHandler : IMessageHandler
{
    private string CommandText => _configurator.SetPrivilegeLevelCommandText;
    private readonly IValidatorConfigurator _configurator;
    private const string SetPrivilege = "SetPrivilege";

    public SetPrivilegeLevelCommandHandler(IValidatorConfigurator configurator)
    {
        _configurator = configurator;
    }
    
    public async IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO message, Dictionary<string, IContextEntry> context)
    {
        if(message.Text == null || message.Text.Split()[0]!=CommandText)
            yield break;

        var arguments = message.Text?.Split();

        if (message.ReplyToMessage == null || arguments.Length != 2 || !int.TryParse(arguments[1], out var targetPrivilege))
        {
            yield return new WriteActionWithDelayedRemove(_configurator.InvalidArgument, message.ChatId, DateTime.UtcNow.OffsetInSeconds(10));
            yield return new DelayedActionWrapper<DeleteMessageActionDto>(new DeleteMessageActionDto(message.MessageId),
                DateTime.UtcNow.OffsetInSeconds(10));
            yield break;
        }

        var entry = new SetPrivilegeLevelContextEntry();
        context[SetPrivilege] = entry;
        entry.AddValue(new SetPrivilegeLevelDto(message.Member, message.ReplyToMessage.Member, int.Parse(arguments[1]), message));
    }
}