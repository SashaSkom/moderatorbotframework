using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;

namespace Handlers.DefaultValidator.ContextEntries;

public class SetPrivilegeLevelContextEntry : IContextEntry
{
    private List<SetPrivilegeLevelDto> Values;

    public SetPrivilegeLevelContextEntry()
    {
        Values = new List<SetPrivilegeLevelDto>();
    }

    public void AddValue(SetPrivilegeLevelDto value)
    {
        Values.Add(value);
    }

    public IEnumerable<SetPrivilegeLevelDto> GetValues()
    {
        return Values;
    }
}

public class SetPrivilegeLevelDto
{
    public Member Initiator;
    public Member Victim;
    public int TargetPl;
    public MessageDTO CommandMessage;

    public SetPrivilegeLevelDto(Member initiator, Member victim, int targetPl, MessageDTO commandMessage)
    {
        Initiator = initiator;
        Victim = victim;
        TargetPl = targetPl;
        CommandMessage = commandMessage;
    }
}