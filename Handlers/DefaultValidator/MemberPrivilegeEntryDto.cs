using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataBasedCollection;

namespace Handlers.DefaultValidator;
[Table("Privileges")]
public sealed class MemberPrivilegeEntryDto : HavingKey<long>
{
    [NotMapped] public long MemberId => Key;
    public PrivilegeLevel PrivilegeLevel { get; private set; }
    [Key]
    [Column("MemberId")]
    public override long Key { get; set; }
    public MemberPrivilegeEntryDto(long key, PrivilegeLevel privilegeLevel)
    {
        PrivilegeLevel = privilegeLevel;
        Key = key;
    }
}