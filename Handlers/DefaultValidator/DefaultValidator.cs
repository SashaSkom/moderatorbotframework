using DataBasedCollection;
using Handlers.DefaultValidator.ContextEntries;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;
using UsefulExtensions;

namespace Handlers.DefaultValidator;

[RequiredOptional("SetPrivilege")]
[Implementing("MessageValidationInfo")]
public class DefaultValidator : IMessageHandler
{
    private ISafeCollection<MemberPrivilegeEntryDto, long> _privileges;
    private const string SetPrivilegeLevel = "SetPrivilege";

    public DefaultValidator(ISafeCollection<MemberPrivilegeEntryDto, long> privileges)
    {
        _privileges = privileges;
    }

    public IEnumerable<IActionDto> HandleContext(MessageDTO messageDto,
                                                 Dictionary<string, IContextEntry> contextEntries)
    {
        if (contextEntries.NotContainsKey(SetPrivilegeLevel))
            yield break;
        var values = (SetPrivilegeLevelContextEntry) contextEntries[SetPrivilegeLevel];
        foreach (var setPrivilegeLevelDto in values.GetValues())
        {
            if (GetPrivilegeLevel(setPrivilegeLevelDto.Initiator) -
                Math.Max(GetPrivilegeLevel(setPrivilegeLevelDto.Victim), setPrivilegeLevelDto.TargetPl) >= 1)
            {
                Add(setPrivilegeLevelDto.Victim, setPrivilegeLevelDto.TargetPl);
                yield return new WriteMessageActionDto(
                    $"@{setPrivilegeLevelDto.Victim.Name}, теперь ты {GetPrivilegeLevelName(setPrivilegeLevelDto.TargetPl)}",
                    messageDto.ChatId);
            }
            else
            {
                yield return new DelayedActionWrapper<DeleteMessageActionDto>(
                    new DeleteMessageActionDto(setPrivilegeLevelDto.CommandMessage.MessageId),
                    DateTime.Now.OffsetInSeconds(15));
                yield return new WriteActionWithDelayedRemove(
                    $"Недостаточный уровень привелегий",
                    messageDto.ChatId, DateTime.Now.OffsetInSeconds(15));
            }
        }
    }

    public int GetPrivilegeLevel(Member member)
    {
        return GetPrivilegeLevel(member.Id);
    }

    public int GetPrivilegeLevel(long member)
    {
        if (!_privileges.Find(member, out var entry))
            return 0;
        return (int) entry.PrivilegeLevel;
    }

    public string GetPrivilegeLevelName(Member member)
    {
        return ((PrivilegeLevel) GetPrivilegeLevel(member)).ToString();
    }

    public string GetPrivilegeLevelName(int privilege)
    {
        return ((PrivilegeLevel) privilege).ToString();
    }

    public void Add(Member member, int privilegeLevel)
    {
        _privileges.AddExclusiveAsync(new MemberPrivilegeEntryDto(member.Id, (PrivilegeLevel) privilegeLevel));
    }

    public void Add(long member, int privilegeLevel)
    {
        _privileges.AddExclusiveAsync(new MemberPrivilegeEntryDto(member, (PrivilegeLevel) privilegeLevel));
    }

    //ToDo configuration

    public async IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO message, Dictionary<string, IContextEntry> context)
    {
        var messageSenderPrivilege = GetPrivilegeLevel(message.Member);
        var messageSenderPrivilegeName = GetPrivilegeLevelName(message.Member);
        var messageVictimPrivilege = 0;
        string messageVictimPrivilegeName = default;
        if (message.ReplyToMessage != null)
        {
            messageVictimPrivilege = GetPrivilegeLevel(message.ReplyToMessage.Member);
            messageVictimPrivilegeName = GetPrivilegeLevelName(message.ReplyToMessage.Member);
        }


        context["MessageValidationInfo"] = new MessageValidationInfoContextEntry
        {
            MessageSenderPrivilegeLevel = messageSenderPrivilege,
            MessageSenderPrivilegeLevelName = messageSenderPrivilegeName,
            MessageVictimPrivilegeLevel = messageVictimPrivilege,
            MessageVictimPrivilegeLevelName = messageVictimPrivilegeName
        };
        foreach (var actionDto in HandleContext(message, context).ToList())
        {
            yield return actionDto;
        }
    }
}