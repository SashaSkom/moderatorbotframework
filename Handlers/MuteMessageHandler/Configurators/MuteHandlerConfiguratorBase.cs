namespace Handlers.MuteMessageHandler.Configurators;

public abstract class MuteHandlerConfiguratorBase : IMuteHandlerConfigurator
{
    protected MuteHandlerConfiguratorBase(string muteCommandText, string unmuteCommandText)
    {
        MuteCommandText = muteCommandText;
        UnmuteCommandText = unmuteCommandText;
    }

    public abstract string InvalidArgument { get; }
    public abstract string PrivilegeLevel { get; }
    public abstract Func<string, int, string> ToMutedPerson { get; }
    public abstract Func<string, string> Mercy { get; }
    public virtual string MuteCommandText { get; }
    public virtual string UnmuteCommandText { get; }
}