﻿namespace Handlers.MuteMessageHandler.Configurators;

public class MuteHandlerConfigurator : MuteHandlerConfiguratorBase
{
    public override string InvalidArgument { get; }
    public override string PrivilegeLevel { get; }
    public override Func<string, int, string> ToMutedPerson { get; }
    public override Func<string, string> Mercy { get; }

    public MuteHandlerConfigurator(string invalidArgument,
                                   string privilegeLevel,
                                   Func<string, int, string> toMutedPerson,
                                   Func<string, string> mercy,
                                   string muteCommandText,
                                   string unmuteCommandText) : base(muteCommandText, unmuteCommandText)
    {
        InvalidArgument = invalidArgument;
        PrivilegeLevel = privilegeLevel;
        ToMutedPerson = toMutedPerson;
        Mercy = mercy;
    }
}