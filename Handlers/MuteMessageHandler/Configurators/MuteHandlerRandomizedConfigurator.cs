﻿namespace Handlers.MuteMessageHandler.Configurators;

public class MuteHandlerRandomizedConfigurator : MuteHandlerConfiguratorBase
{
    public override string InvalidArgument => _invalidMessages[_rnd.Next(_invalidMessages.Count)];
    public override string PrivilegeLevel => _privilegeMessages[_rnd.Next(_privilegeMessages.Count)];
    public override Func<string, int, string> ToMutedPerson => _muteMessages[_rnd.Next(_muteMessages.Count)];
    public override Func<string, string> Mercy => _mercyMessages[_rnd.Next(_mercyMessages.Count)];

    private readonly List<string> _invalidMessages;
    private readonly List<Func<string, int, string>> _muteMessages;
    private readonly List<string> _privilegeMessages;
    private readonly List<Func<string, string>> _mercyMessages;
    private readonly Random _rnd;

    public MuteHandlerRandomizedConfigurator(List<string> invalidMessages,
                                             List<Func<string, int, string>> muteMessages,
                                             List<string> privilegeMessages,
                                             List<Func<string, string>> mercyMessages,
                                             string muteCommandText,
                                             string unmuteCommandText)
        : base(muteCommandText, unmuteCommandText)
    {
        _invalidMessages = invalidMessages;
        _muteMessages = muteMessages;
        _privilegeMessages = privilegeMessages;
        _mercyMessages = mercyMessages;
        _rnd = new Random();
    }
}