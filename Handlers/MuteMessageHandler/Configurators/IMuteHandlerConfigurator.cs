﻿namespace Handlers.MuteMessageHandler.Configurators;

public interface IMuteHandlerConfigurator
{
    public string InvalidArgument { get; }
    public string PrivilegeLevel { get; }
    public Func<string, int, string> ToMutedPerson { get; }
    public Func<string, string> Mercy { get; }

    public string MuteCommandText { get; }
    public string UnmuteCommandText { get; }
}