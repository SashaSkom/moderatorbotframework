using Handlers.MuteMessageHandler.ContextEntries;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;

namespace Handlers.MuteMessageHandler;

[RequiredOptional("MembersToMute", "MembersToUnmute")]
public abstract class MuteHandlerBase : IMessageHandler
{
    public virtual IEnumerable<MutedMemberDto> GetMutedMembersFromContext(Dictionary<string, IContextEntry> context)
    {
        return context.ContainsKey("MembersToMute")
            ? ((MuteContextEntry) context["MembersToMute"]).Values
            : Enumerable.Empty<MutedMemberDto>();
    }

    public virtual IEnumerable<long> GetUnmutedMembersFromContext(Dictionary<string, IContextEntry> context)
    {
        return context.ContainsKey("MembersToUnmute")
            ? ((UnmuteContextEntry) context["MembersToUnmute"]).GetValues()
            : Enumerable.Empty<long>();
    }

    public abstract IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO message, Dictionary<string, IContextEntry> context);
}