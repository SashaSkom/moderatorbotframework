using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataBasedCollection;

namespace Handlers.MuteMessageHandler;

[Table("Mutes")]
public sealed class MutedMemberDto : HavingKey<long>
{
    [NotMapped] public long MemberId => Key;
    [Key] [Column("MemberId")] public override long Key { get; set; }
    public long UnmuteDate { get; private set; }

    public MutedMemberDto(long key, long unmuteDate)
    {
        Key = key;
        UnmuteDate = unmuteDate;
    }
}