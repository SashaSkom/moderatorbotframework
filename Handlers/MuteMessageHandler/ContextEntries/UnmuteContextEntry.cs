using ModeratorBotFrameworkCore.MessagePipeline;

namespace Handlers.MuteMessageHandler.ContextEntries;

public class UnmuteContextEntry : ContextEntry<long>
{
    private List<long> membersId;

    public UnmuteContextEntry()
    {
        membersId = new List<long>();
    }

    public void AddValue(long id)
    {
        membersId.Add(id);
    }

    public IEnumerable<long> GetValues()
    {
        foreach (var l in membersId)
        {
            yield return l;
        }
    }
}