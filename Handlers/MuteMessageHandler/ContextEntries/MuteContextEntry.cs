using ModeratorBotFrameworkCore.MessagePipeline;

namespace Handlers.MuteMessageHandler.ContextEntries;

public class MuteContextEntry : ContextEntry<MutedMemberDto>
{
    public List<MutedMemberDto> Values;

    public void AddValue(MutedMemberDto value)
    {
        Values.Add(value);
    }

    public MuteContextEntry()
    {
        Values = new List<MutedMemberDto>();
    }
}