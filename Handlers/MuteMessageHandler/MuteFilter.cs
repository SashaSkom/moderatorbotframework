﻿using DataBasedCollection;
using ModeratorBotFrameworkCore.Bot;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;

namespace Handlers.MuteMessageHandler;

[RequiredOptional("MembersToMute")]
[RequiredBotOptions("DeleteMessage")]
public class MuteFilter : MuteHandlerBase
{
    private readonly ISafeCollection<MutedMemberDto, long> _mutedList;

    private void Add(Member member, DateTime unmuteDate)
    {
        _mutedList.AddAsync(new MutedMemberDto(member.Id, unmuteDate.ToUnixTime()));
    }

    private void Add(MutedMemberDto member)
    {
        _mutedList.AddExclusive(member);
    }

    private void Remove(Member member)
    {
        if (_mutedList.Find(member.Id, out var entry))
            _mutedList.Remove(entry);
    }

    private void Remove(long member)
    {
        if (_mutedList.Find(member, out var entry))
            _mutedList.Remove(entry);
    }

    private void HandleUsersFromContext(Dictionary<string, IContextEntry> context)
    {
        GetMutedMembersFromContext(context).ToList().ForEach(Add);
        GetUnmutedMembersFromContext(context).ToList().ForEach(Remove);
    }

    public MuteFilter(ISafeCollection<MutedMemberDto, long> mutedList)
    {
        _mutedList = mutedList;
    }

    public async override IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO message, Dictionary<string, IContextEntry> context)
    {
        HandleUsersFromContext(context);

        if (!_mutedList.Find(message.Member.Id, out var entry)) yield break;

        if (entry!.UnmuteDate > DateTime.UtcNow.ToUnixTime())
        {
            yield return new DeleteMessageActionDto(message.MessageId);
        }
        else
        {
            Remove(message.Member);
        }
    }
}