using Handlers.DefaultValidator;
using Handlers.MuteMessageHandler.Configurators;
using Handlers.MuteMessageHandler.ContextAction;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;
using UsefulExtensions;

namespace Handlers.MuteMessageHandler.Commands;

[ImplementingOptional("MembersToMute")]
[Required("MessageValidationInfo")]
public class MuteCommandHandler : IMessageHandler
{
    private string CommandText => _configurator.MuteCommandText;
    private readonly IMuteHandlerConfigurator _configurator;
    private const string MembersToMuteContextKey = "MembersToMute";
    private const string MessageValidationInfoContextKey = "MessageValidationInfo";


    public MuteCommandHandler(IMuteHandlerConfigurator configurator)
    {
        _configurator = configurator;
    }

    public async IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO message, Dictionary<string, IContextEntry> context)
    {
        var privilegeInfo = (MessageValidationInfoContextEntry) context[MessageValidationInfoContextKey];
        if (privilegeInfo.MessageSenderPrivilegeLevel < 1 &&
            privilegeInfo.MessageVictimPrivilegeLevel <= privilegeInfo.MessageSenderPrivilegeLevel)
            yield break;
        if (message.Text == null || message.Text.Split()[0] != CommandText)
            yield break;
        
        if (message.ReplyToMessage == null || message.ReplyToMessage.Member.Id == null)
        {
            yield return new WriteActionWithDelayedRemove(_configurator.InvalidArgument,
                message.ChatId, DateTime.UtcNow.OffsetInSeconds(10));
            yield return new DelayedActionWrapper<DeleteMessageActionDto>(new DeleteMessageActionDto(message.MessageId),
                DateTime.UtcNow.OffsetInSeconds(10));
            yield break;
        }

        var commandArgs = message.Text.Split().Skip(1).ToList();
        if (commandArgs.Count == 0 || !int.TryParse(commandArgs[0], out var duration))
        {
            yield return new WriteActionWithDelayedRemove(_configurator.InvalidArgument,
                message.ChatId, DateTime.UtcNow + new TimeSpan(0, 0, 0, 10));
            yield return new DelayedActionWrapper<DeleteMessageActionDto>(new DeleteMessageActionDto(message.MessageId),
                DateTime.UtcNow.OffsetInSeconds(10));
            yield break;
        }
        //ToDo валидация

        new TimedMuteAction(new TimeSpan(duration, 0, 0, 0)).Realize(context, new[] {message.ReplyToMessage.Member});
        yield return
            new WriteMessageActionDto(_configurator.ToMutedPerson(message.ReplyToMessage.Member.Name, duration),
                message.ChatId);
    }
}