using Handlers.DefaultValidator;
using Handlers.MuteMessageHandler.Configurators;
using Handlers.MuteMessageHandler.ContextEntries;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;
using UsefulExtensions;

namespace Handlers.MuteMessageHandler.Commands;

[Required("MessageValidationInfo")]
[ImplementingOptional("MembersToUnmute")]
public class UnmuteCommandHandler : IMessageHandler
{
    private const string MembersToUnmuteContextKey = "MembersToUnmute";
    private const string MessageValidationInfoContextKey = "MessageValidationInfo";
    private readonly IMuteHandlerConfigurator _configurator;
    private string CommandText => _configurator.UnmuteCommandText;

    public UnmuteCommandHandler(IMuteHandlerConfigurator configurator)
    {
        _configurator = configurator;
    }

    public async IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO message,
                                                            Dictionary<string, IContextEntry> context)
    {
        var privilegeInfo = (MessageValidationInfoContextEntry) context[MessageValidationInfoContextKey];

        if (message.Text == null || message.Text.Split()[0] != CommandText) yield break;
        if (message.ReplyToMessage == null || message.ReplyToMessage.Member.Id == null)
        {
            yield return new WriteActionWithDelayedRemove(_configurator.InvalidArgument, message.ChatId,
                DateTime.UtcNow.OffsetInSeconds(10));
            yield return new DelayedActionWrapper<DeleteMessageActionDto>(new DeleteMessageActionDto(message.MessageId),
                DateTime.UtcNow.OffsetInSeconds(10));
            yield break;
        }

        if (privilegeInfo.MessageSenderPrivilegeLevel < 1 &&
            privilegeInfo.MessageSenderPrivilegeLevel - privilegeInfo.MessageVictimPrivilegeLevel < 1)
        {
            yield return new WriteMessageActionDto(_configurator.PrivilegeLevel, message.ChatId);
            yield break;
        }

        var contextEntry = new UnmuteContextEntry();
        if (context.NotContainsKey(MembersToUnmuteContextKey))
            context[MembersToUnmuteContextKey] = contextEntry;
        contextEntry.AddValue(message.ReplyToMessage.Member.Id);
        yield return
            new WriteMessageActionDto(_configurator.Mercy(message.ReplyToMessage.Member.Name), message.ChatId);
    }
}