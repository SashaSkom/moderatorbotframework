using Handlers.MuteMessageHandler.ContextEntries;
using ModeratorBotFrameworkCore.Bot;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.IContextAction;

namespace Handlers.MuteMessageHandler.ContextAction;
[ImplementingOptional("MembersToMute")]
public class PermanentMuteAction : IMemberAction
{
    public void Realize(Dictionary<string, IContextEntry> context, IEnumerable<Member> members)
    {
        if(!context.ContainsKey("MembersToMute"))
            context["MembersToMute"] = new MuteContextEntry();
        if (context.ContainsKey("MembersToMute"))
        {
            members.Select(x => new MutedMemberDto(x.Id,DateTime.MaxValue.ToUnixTime()))
                  .ToList()
                  .ForEach(x => ((MuteContextEntry) context["MembersToMute"]).AddValue(x));
        }
    }
}