using Handlers.MuteMessageHandler.ContextEntries;
using ModeratorBotFrameworkCore.Bot;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.IContextAction;
using UsefulExtensions;

namespace Handlers.MuteMessageHandler.ContextAction;

[ImplementingOptional("MembersToMute")]
public class TimedMuteAction : IMemberAction
{
    private readonly TimeSpan _muteTime;

    public TimedMuteAction(TimeSpan muteTime)
    {
        _muteTime = muteTime;
    }

    public void Realize(Dictionary<string, IContextEntry> context, IEnumerable<Member> members)
    {
        if (context.NotContainsKey("MembersToMute"))
            context["MembersToMute"] = new MuteContextEntry();
        members
            .Select(member => new MutedMemberDto(member.Id, (DateTime.UtcNow + _muteTime).ToUnixTime()))
            .ToList()
            .ForEach(x => ((MuteContextEntry) context["MembersToMute"]).AddValue(x));
    }
}