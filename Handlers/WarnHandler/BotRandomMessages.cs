﻿namespace Handlers.WarnHandler;

public class WarnBotRandomMessages : IWarnHandlerConfigurator
{
    public Func<string, int, string> Warning => _warningMessages[_rnd.Next(_warningMessages.Count)];
    public Func<string, string> Mercy => _mercyMessages[_rnd.Next(_mercyMessages.Count)];
    public string IncorrectArguments { get; }
    public string CommandText { get; }
    public string ClearCommandText { get; }

    private readonly List<Func<string, int, string>> _warningMessages;
    private readonly List<Func<string, string>> _mercyMessages;
    private readonly Random _rnd;

    public WarnBotRandomMessages(List<Func<string, int, string>> warningMessages,
                                 List<Func<string, string>> mercyMessages,
                                 string commandText,
                                 string clearCommandText, string incorrectArguments)
    {
        _warningMessages = warningMessages;
        _mercyMessages = mercyMessages;
        CommandText = commandText;
        ClearCommandText = clearCommandText;
        IncorrectArguments = incorrectArguments;
        _rnd = new Random();
    }
}