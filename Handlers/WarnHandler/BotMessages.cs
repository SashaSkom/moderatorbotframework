﻿namespace Handlers.WarnHandler;

public class WarnHandlerConfigurator : IWarnHandlerConfigurator
{
    public string IncorrectArguments { get; }
    public string CommandText { get; }
    public Func<string, string> Mercy { get; }
    public Func<string, int, string> Warning { get; }

    public string ClearCommandText { get; }

    public WarnHandlerConfigurator(Func<string, int, string> warnMessage, 
                           Func<string, string> mercyMessage, 
                           string commandText,
                           string clearCommandText, string incorrectArguments)
    {
        CommandText = commandText;
        ClearCommandText = clearCommandText;
        IncorrectArguments = incorrectArguments;
        Warning = warnMessage;
        Mercy = mercyMessage;
    }
}