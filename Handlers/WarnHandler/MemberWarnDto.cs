using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataBasedCollection;

namespace Handlers.WarnHandler;

[Table("Warns")]
public sealed class MemberWarnDto : HavingKey<long>
{
    public MemberWarnDto(int warnsCount, long key)
    {
        WarnsCount = warnsCount;
        Key = key;
    }

    [Key] public long MemberId => Key;
    [Key] [Column("MemberId")] public override long Key { get; set; }
    public int WarnsCount { get; set; }
}