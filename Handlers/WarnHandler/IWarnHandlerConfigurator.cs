﻿namespace Handlers.WarnHandler;

public interface IWarnHandlerConfigurator
{
    public Func<string, int, string> Warning { get; }
    public Func<string, string> Mercy { get; }
    
    public string IncorrectArguments { get; }
    public string CommandText { get; }
    public string ClearCommandText { get; }
}