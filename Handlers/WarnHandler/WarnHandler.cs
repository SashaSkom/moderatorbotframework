using DataBasedCollection;
using Handlers.DefaultValidator;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.IContextAction;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;
using UsefulExtensions;

namespace Handlers.WarnHandler;

[Required("MessageValidationInfo")]
[ImplementingOptional("MembersToMute")]
public class WarnHandler : IMessageHandler
{
    private string WarnCommandText => _configuredMessages.CommandText;
    private string ClearCommandText => _configuredMessages.ClearCommandText;
    private readonly ISafeCollection<MemberWarnDto, long> _warns;
    private readonly Dictionary<int, IMemberAction> _punishments;
    private readonly IWarnHandlerConfigurator _configuredMessages;

    public WarnHandler(ISafeCollection<MemberWarnDto, long> warns,
                       Dictionary<int, IMemberAction> punishments,
                       IWarnHandlerConfigurator configuredMessages)
    {
        _warns = warns;
        _punishments = punishments;
        _configuredMessages = configuredMessages;
    }

    private IEnumerable<IActionDto> HandleWarnCommand(Dictionary<string, IContextEntry> context, MessageDTO messageDto)
    {
        var privilegeInfo = (MessageValidationInfoContextEntry) context["MessageValidationInfo"];

        if (messageDto.Text==null
            ||messageDto.Text.Split()[0] != WarnCommandText
            || privilegeInfo.MessageSenderPrivilegeLevel == 0
            || privilegeInfo.MessageVictimPrivilegeLevel != 0)
            yield break;
        
        if (messageDto.ReplyToMessage == null)
        {
            yield return new WriteActionWithDelayedRemove(_configuredMessages.IncorrectArguments, messageDto.ChatId, DateTime.UtcNow.OffsetInSeconds(10));
            yield return new DelayedActionWrapper<DeleteMessageActionDto>(
                new DeleteMessageActionDto(messageDto.MessageId),
                DateTime.UtcNow.OffsetInSeconds(10));
            yield break;
        }

        var warnsCount = _warns.Find(messageDto.ReplyToMessage.Member.Id, out var warns)
            ? warns!.WarnsCount + 1
            : 1;

        if (_warns.Find(messageDto.ReplyToMessage.Member.Id, out var entry))
        {
            _warns.AddExclusiveAsync(new MemberWarnDto(entry!.WarnsCount + 1, entry.Key));
        }
        else
        {
            _warns.AddAsync(new MemberWarnDto(1, messageDto.ReplyToMessage.Member.Id));
        }

        _punishments.Keys
                    .Where(x => x == warnsCount)
                    .ToList()
                    .ForEach(x => _punishments[x]
                        .Realize(context, new[] {messageDto.ReplyToMessage.Member}));

        yield return new WriteMessageActionDto(
            _configuredMessages.Warning(messageDto.ReplyToMessage.Member.Name, warnsCount),
            messageDto.ChatId);
    }

    private IEnumerable<IActionDto> HandleClearCommand(Dictionary<string, IContextEntry> context, MessageDTO messageDto)
    {
        var privilegeInfo = (MessageValidationInfoContextEntry) context["MessageValidationInfo"];
        if (messageDto.Text != null
            && messageDto.Text.Split()[0] != ClearCommandText)
            yield break;
        if (privilegeInfo.MessageSenderPrivilegeLevel == 0
            || privilegeInfo.MessageVictimPrivilegeLevel > 1)
            yield break;
        if (messageDto.ReplyToMessage == null)
            yield break;

        if (_warns.Find(messageDto.ReplyToMessage.Member.Id, out var entry))
        {
            entry!.WarnsCount = 0;
        }

        yield return new WriteMessageActionDto(
            _configuredMessages.Mercy(messageDto.ReplyToMessage.Member.Name),
            messageDto.ChatId);
    }

    public async IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO message,
                                                            Dictionary<string, IContextEntry> context)
    {
        foreach (var actionDto in HandleWarnCommand(context, message).Concat(HandleClearCommand(context, message)))
        {
            yield return actionDto;
        }
    }
}