using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.IContextAction;

namespace Handlers.LogHandler;

[ImplementingOptional("Logs")]
public class LogsContextAction : IContextAction<(Type, string)>
{
    public void Realize(Dictionary<string, IContextEntry> context, (Type, string) members)
    {
        ((MessageLogsContextEntry) context["MessageLogsInfo"]).AddLog(members);
    }
}