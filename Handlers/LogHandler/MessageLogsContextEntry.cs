﻿using ModeratorBotFrameworkCore.MessagePipeline;

namespace Handlers.LogHandler;

public class MessageLogsContextEntry : ContextEntry<string>
{
    public List<(Type, string)> Logs;

    public void AddLog((Type, string) log)
    {
        Logs.Add(log);
    }

    public MessageLogsContextEntry()
    {
        Logs = new List<(Type, string)>();
    }
}