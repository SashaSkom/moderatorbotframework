﻿using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;
using UsefulExtensions;

namespace Handlers.LogHandler
{
    [Implementing("MessageLogsInfo")]
    public class LogsImlementingHandler : IMessageHandler
    {
        public async IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO message, Dictionary<string, IContextEntry> context)
        {
            if (context.NotContainsKey("MessageLogsInfo"))
            {
                context["MessageLogsInfo"] = new MessageLogsContextEntry();
            }

            yield break;
        }
    }
}
