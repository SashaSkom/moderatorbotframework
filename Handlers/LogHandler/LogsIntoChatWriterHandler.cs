using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;

namespace Handlers.LogHandler;
[Required("MessageLogsInfo")]
public class LogsIntoChatWriterHandler : IMessageHandler
{
    private readonly long _chatId;

    public LogsIntoChatWriterHandler(long chatId)
    {
        _chatId = chatId;
    }

    public async IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO message, Dictionary<string, IContextEntry> context)
    {
        var logs = ((MessageLogsContextEntry) context["MessageLogsInfo"]).Logs;
        foreach (var log in logs)
        {
            yield return new WriteMessageActionDto($"{log.Item1}: {log.Item2}", _chatId);
        }
    }
}