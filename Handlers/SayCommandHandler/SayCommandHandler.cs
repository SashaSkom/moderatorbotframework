﻿using Handlers.DefaultValidator;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;
using UsefulExtensions;

namespace Handlers.SayCommandHandler;

[RequiredOptional("MessageValidationInfo")]
public class SayCommandHandler : IMessageHandler
{
    private readonly long _chatId;
    private readonly string _commandText;

    public SayCommandHandler(long chatId, string commandText)
    {
        _chatId = chatId;
        _commandText = commandText;
    }

    public async IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO message, Dictionary<string, IContextEntry> context)
    {
        if (message.Text != null && message.Text.Split()[0] == _commandText && IsCommandValid(context))
            yield return new WriteMessageActionDto(string.Join(' ', message.Text.Split()[1..]), _chatId);
    }

    private static bool IsCommandValid(Dictionary<string, IContextEntry> contextEntries)
    {
        if (contextEntries.NotContainsKey("MessageValidationInfo"))
            return true;
        var validationInfo = (MessageValidationInfoContextEntry) contextEntries["MessageValidationInfo"];
        return validationInfo.MessageSenderPrivilegeLevel > 1;
    }
}