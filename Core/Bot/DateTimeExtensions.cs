﻿namespace ModeratorBotFrameworkCore.Bot;
public static class DateTimeExtensions
{
    public static long ToUnixTime(this DateTime time)
    {
        var sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        return (long)(time - sTime).TotalSeconds;
    }
}
