using ModeratorBotFrameworkCore.Bot.DTO;

namespace ModeratorBotFrameworkCore.Bot.Actions;

public class WriteMessageWithOnDemandDelete : InvocableAction
{
    private readonly string _text;
    private readonly long _messageChatId;
    public bool IsMessageWritten => _messageToDelete != null;
    private MessageDTO? _messageToDelete;
    private IPlugin? _bot;
    public override bool IsActionReady => _messageToDelete != null;


    public WriteMessageWithOnDemandDelete(string text, long messageChatId)
    {
        _text = text;
        _messageChatId = messageChatId;
    }

    public override async Task Visit(IPlugin bot)
    {
        var message = await bot.WriteMessage(new WriteMessageActionDto(_text, _messageChatId));
        _messageToDelete = message;
        _bot = bot;
    }

    public override void InitiateAction()
    {
        if (_messageToDelete != null) _bot.DeleteMessage(new DeleteMessageActionDto(_messageToDelete.MessageId));
        else
            throw new ApplicationException("can't delete message");
    }
}