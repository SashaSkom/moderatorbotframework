﻿using ModeratorBotFrameworkCore.Bot.DTO;

namespace ModeratorBotFrameworkCore.Bot.Actions;

public class SendPictureWithMessageWithOnDemandDelete : InvocableAction
{
    private readonly string _text;
    private readonly string _pathToImg;
    private readonly long _messageChatId;
    public bool IsMessageWritten => _messageToDelete != null;
    private MessageDTO? _messageToDelete;
    private IPlugin? _bot;

    public SendPictureWithMessageWithOnDemandDelete(string pathToImg, string text, long messageChatId)
    {
        _pathToImg = pathToImg;
        _text = text;
        _messageChatId = messageChatId;
    }

    public override void InitiateAction()
    {
        _bot.DeleteMessage(new DeleteMessageActionDto(_messageToDelete.MessageId));
    }

    public override async Task Visit(IPlugin bot)
    {
        var message =
            bot.SendPictureWithMessage(new SendPictureWithMessageActionDto(_pathToImg, _text, _messageChatId));
        _messageToDelete = message;
        _bot = bot;
    }

    public override bool IsActionReady => _messageToDelete != null;
}