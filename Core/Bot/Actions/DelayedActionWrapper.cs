namespace ModeratorBotFrameworkCore.Bot.Actions;

public class DelayedActionWrapper<T> : IActionDto where T : IActionDto
{
    private readonly T _actionDto;
    private readonly DateTime _time;
    public DelayedActionWrapper(T actionDto, DateTime time)
    {
        _actionDto = actionDto;
        _time = time;
    }
    public async Task Visit(IPlugin bot)
    {
        bot.SetDelayedAction(_actionDto, _time);
    }
}