﻿namespace ModeratorBotFrameworkCore.Bot.Actions;

public abstract class SendPictureActionDto : IActionDto
{
    public readonly string PathToImg;
    public readonly long MessageChatId;

    protected SendPictureActionDto(string pathToImg, long messageChatId)
    {
        PathToImg = pathToImg;
        MessageChatId = messageChatId;
    }

    public async Task Visit(IPlugin bot)
    {
        bot.SendPicture(this);
    }
}