
namespace ModeratorBotFrameworkCore.Bot.Actions;

public class DeleteMessageActionDto : IActionDto
{
    public string MessageId;

    public DeleteMessageActionDto(string messageId)
    {
        MessageId = messageId;
    }

    public async Task Visit(IPlugin bot)
    {
        try
        {
            await bot.DeleteMessage(this);
        }
        catch
        {
            // ignored
        }
    }
}