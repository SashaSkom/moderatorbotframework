namespace ModeratorBotFrameworkCore.Bot.Actions;

public class WriteActionWithDelayedRemove : IActionDto
{
    private readonly string _text;
    private readonly long _messageChatId;
    private readonly DateTime _time;

    public WriteActionWithDelayedRemove(string text, long messageChatId, DateTime time)
    {
        _text = text;
        _messageChatId = messageChatId;
        _time = time;
    }

    public async Task Visit(IPlugin bot)
    {
        var message = await bot.WriteMessage(new WriteMessageActionDto(_text, _messageChatId));
        bot.AcceptAction(new DelayedActionWrapper<DeleteMessageActionDto>(new DeleteMessageActionDto(message.MessageId), _time));
    }
}