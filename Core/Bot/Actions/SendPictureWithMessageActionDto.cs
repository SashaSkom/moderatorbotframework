﻿namespace ModeratorBotFrameworkCore.Bot.Actions;

public class SendPictureWithMessageActionDto : IActionDto
{
    public readonly string PathToImg;
    public readonly string Text;
    public readonly long MessageChatId;

    public SendPictureWithMessageActionDto(string pathToImg, string text, long messageChatId)
    {
        PathToImg = pathToImg;
        Text = text;
        MessageChatId = messageChatId;
    }

    public async Task Visit(IPlugin bot)
    {
        bot.SendPictureWithMessage(this);
    }
}