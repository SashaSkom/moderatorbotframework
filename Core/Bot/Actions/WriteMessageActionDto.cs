namespace ModeratorBotFrameworkCore.Bot.Actions;

public class WriteMessageActionDto : IActionDto
{
    public readonly string Text;
    public readonly long MessageChatId;

    public WriteMessageActionDto(string text, long messageChatId)
    {
        Text = text;
        MessageChatId = messageChatId;
    }

    public async Task Visit(IPlugin bot)
    {
        bot.WriteMessage(this);
    }
    
}