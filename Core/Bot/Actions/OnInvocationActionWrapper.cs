namespace ModeratorBotFrameworkCore.Bot.Actions;

public class OnInvocationActionWrapper<TAction> : InvocableAction where TAction : IActionDto
{
    private TAction _actionDto;
    private IPlugin? _bot;
    public override bool IsActionReady => _bot != null;

    public OnInvocationActionWrapper(TAction actionDto)
    {
        _actionDto = actionDto;
    }

    public override async Task Visit(IPlugin bot)
    {
        _bot = bot;
    }

    public override void InitiateAction()
    {
        if (_bot != null) _bot.AcceptAction(_actionDto);
        else
        {
            throw new ApplicationException("Action was not ready");
        }
    }
}