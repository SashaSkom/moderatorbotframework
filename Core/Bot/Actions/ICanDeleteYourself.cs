﻿namespace ModeratorBotFrameworkCore.Bot.Actions
{
    public abstract class InvocableAction : IActionDto
    {
        public abstract void InitiateAction();
        public abstract Task Visit(IPlugin bot);

        public abstract bool IsActionReady { get; }

        public virtual async void InitiateActionAsync()
        {
            while (true)
            {
                if (IsActionReady)
                {
                    InitiateAction();
                    break;
                }

                await Task.Delay(1000);
            }
        }
    }
}