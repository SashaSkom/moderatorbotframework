namespace ModeratorBotFrameworkCore.Bot.Actions;

public interface IActionDto
{
    Task Visit(IPlugin bot);
}