
using DataBasedCollection;

namespace ModeratorBotFrameworkCore.Bot.DTO;
public sealed class Member : HavingKey<long>
{
    public long Id => Key;
    
    private string _name;

    public string Name
    {
        get => _name is null ? Id.ToString() : _name;
        set => _name = value; 
    }

    public Member(long id, string name)
    {
        Key = id;
        Name = name;
    }
}
