namespace ModeratorBotFrameworkCore.Bot.DTO;

public class MessageDTO
{
    public string? Text;
    public string? PathToImg;
    public string? MessageId;
    public Member Member;
    public MessageDTO? ReplyToMessage;
    public long ChatId;
}