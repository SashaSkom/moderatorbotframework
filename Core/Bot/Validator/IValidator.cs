using ModeratorBotFrameworkCore.Bot.DTO;

namespace ModeratorBotFrameworkCore.Bot.Validator;

public interface IValidator
{
    public int GetPrivilegeLevel(Member member);
    public int GetDeltaPrivilege(Member member, Member member2);
    public string GetPrivilegeLevelName(Member member);
    public string GetPrivilegeLevelName(int level);

    public void Add(Member member, int privilegeLevel);
    
}