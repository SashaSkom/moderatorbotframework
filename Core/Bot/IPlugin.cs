using System.Collections.Concurrent;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;

namespace ModeratorBotFrameworkCore.Bot;

public abstract class IPlugin
{
    public List<Func<MessageDTO, IAsyncEnumerable<IActionDto>>> Observers;

    public void Connect(Func<MessageDTO, IAsyncEnumerable<IActionDto>> func)
    {
        Observers.Add(func);
    }

    public IPlugin()
    {
        Observers = new List<Func<MessageDTO, IAsyncEnumerable<IActionDto>>>();
        actions = new ConcurrentDictionary<IActionDto, DateTime>();
        DelayedActionsExecutor();
    }

    public virtual Task DeleteMessage(DeleteMessageActionDto target)
    {
        throw new NotImplementedException();
    }

    public abstract Task<MessageDTO?> WriteMessage(WriteMessageActionDto target);
    public abstract Task AcceptAction(IActionDto action);

    public virtual MessageDTO? SendPicture(SendPictureActionDto sendPictureActionDto)
    {
        throw new NotImplementedException();
    }

    public virtual ConcurrentDictionary<IActionDto, DateTime> actions { get; set; }

    public virtual void SetDelayedAction(IActionDto action, DateTime time)
    {
        actions[action] = time;
        Console.WriteLine(action);
    }

    public async Task DelayedActionsExecutor()
    {
        while (true)
        {
            //Console.WriteLine("cycle started");
            foreach (var actionDto in actions.Keys.Where(x => actions[x] < DateTime.UtcNow))
            {
                AcceptAction(actionDto);
            }

            await Task.Delay(1000);
        }
    }

    public virtual MessageDTO? SendPictureWithMessage(SendPictureWithMessageActionDto sendPictureActionDto)
    {
        throw new NotImplementedException();
    }
}