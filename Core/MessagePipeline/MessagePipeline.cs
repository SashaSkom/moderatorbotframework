using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;

namespace ModeratorBotFrameworkCore.MessagePipeline;

public class MessagePipeline
{
    private readonly List<IMessageHandler> _handlers;
    private long _chatId;

    public MessagePipeline(DependencyResolver dependencyResolver, long chatId, params IMessageHandler[] handlers)
    {
        _chatId = chatId;
        _handlers = handlers.ToList();
        _handlers = dependencyResolver.ResolveDependencies(_handlers);
    }

    public async IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO messageDto)
    {
        if(messageDto.ChatId!=_chatId)
            yield break;
        var messageWorkResult = new Dictionary<string, IContextEntry>();
        foreach (var handler in _handlers)
        {
            await foreach (var action in handler.HandleMessage(messageDto, messageWorkResult))
            {
                yield return action;
            }
        }
    }
}