using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;

namespace ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;

public interface IMessageHandler
{
    public IAsyncEnumerable<IActionDto> HandleMessage(MessageDTO message, Dictionary<string, IContextEntry> context);
}