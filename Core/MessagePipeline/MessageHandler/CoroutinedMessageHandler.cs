using System.Collections.Concurrent;
using ModeratorBotFrameworkCore.Bot.Actions;
using ModeratorBotFrameworkCore.Bot.DTO;
using UsefulExtensions;

namespace ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;

public abstract class CoroutinedMessageHandler : IMessageHandler
{
    private ConcurrentDictionary<IEnumerator<WaitForSeconds>, DateTime> Coroutines = new();
    private bool CycleStarted;

    public abstract IAsyncEnumerable<IActionDto>
        HandleMessage(MessageDTO message, Dictionary<string, IContextEntry> context);

    protected async Task StartCoroutine(Func<IEnumerable<WaitForSeconds>> coroutine)
    {
        var ien = coroutine().GetEnumerator();
        ien.MoveNext();
        Coroutines[ien] = new DateTime((DateTime.UtcNow.OffsetInSeconds(ien.Current.Seconds)).Ticks);
        if (!CycleStarted)
        {
            Cycle();
            CycleStarted = true;
        }

        Console.WriteLine("Coroutine Added");
        foreach (var dateTime in Coroutines)
        {
            Console.WriteLine(dateTime);
        }
    }

    private async Task Cycle()
    {
        while (true)
        {
            HandleCoroutines();
            await Task.Delay(1000);
        }
    }

    private async Task HandleCoroutines()
    {
        var invTime = DateTime.UtcNow;
        foreach (var keyValuePair in Coroutines)
        {
            if (keyValuePair.Value.Ticks > invTime.Ticks) 
                continue;
            if (!keyValuePair.Key.MoveNext())
                Coroutines.Remove(keyValuePair.Key, out _);
        }

        Coroutines.Keys
                  .Where(x => Coroutines[x] <= invTime)
                  .ToList()
                  .ForEach(x =>
                      Coroutines[x] = new DateTime((DateTime.UtcNow + new TimeSpan(0, 0, 0, x.Current.Seconds)).Ticks));
    }
}