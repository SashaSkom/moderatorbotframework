namespace ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;

public class WaitForSeconds
{
    public readonly int Seconds;

    public WaitForSeconds(int seconds)
    {
        Seconds = seconds;
    }
}