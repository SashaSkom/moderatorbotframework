namespace ModeratorBotFrameworkCore.MessagePipeline.Attributes;

public class BaseAttribute : Attribute
{
    public string[] Parameters;

    public BaseAttribute(params string[] parameters)
    {
        Parameters = parameters;
    }
}