namespace ModeratorBotFrameworkCore.MessagePipeline.Attributes;

public class RequiredBotOptionsAttribute : BaseAttribute
{
    public string[] deps;
    public RequiredBotOptionsAttribute(params string[] methods)
    {
        this.deps = methods;
    }
}