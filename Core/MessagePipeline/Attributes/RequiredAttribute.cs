namespace ModeratorBotFrameworkCore.MessagePipeline.Attributes;

public class RequiredAttribute : BaseAttribute
{
    public RequiredAttribute(params string[] parameters) : base(parameters)
    {
        
    }
}