namespace ModeratorBotFrameworkCore.MessagePipeline.Attributes;

public class RequiredOptionalAttribute : BaseAttribute
{
    public RequiredOptionalAttribute(params string[] parameters) : base(parameters)
    {
        
    }
}