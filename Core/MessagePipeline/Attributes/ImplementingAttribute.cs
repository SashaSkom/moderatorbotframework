namespace ModeratorBotFrameworkCore.MessagePipeline.Attributes;

public class ImplementingAttribute : BaseAttribute
{
    public ImplementingAttribute(params string[] parameters) : base(parameters)
    {
        
    }
}