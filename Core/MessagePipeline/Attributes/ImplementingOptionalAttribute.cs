namespace ModeratorBotFrameworkCore.MessagePipeline.Attributes;

public class ImplementingOptionalAttribute : BaseAttribute
{
    public ImplementingOptionalAttribute(params string[] parameters) : base(parameters)
    {
        
    }
}