using ModeratorBotFrameworkCore.Bot.DTO;

namespace ModeratorBotFrameworkCore.MessagePipeline;

public class MessageHandlingContext
{
    public MessageDTO contextBase;
}

public class DefaultContext : MessageHandlingContext
{
    public string messageSenderPrivilege;
    public string messageVictimPrivilege;
}