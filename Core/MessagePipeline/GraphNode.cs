namespace ModeratorBotFrameworkCore.MessagePipeline;

public class GraphNode<TValue>
{
    public TValue value;
    //public List<GraphNode<TValue>> outPoints;
    public List<GraphNode<TValue>> inNodes;

    public GraphNode(TValue value, List<GraphNode<TValue>> outPoints, List<GraphNode<TValue>> inNodes)
    {
        this.value = value;
        //this.outPoints = outPoints;
        this.inNodes = inNodes;
    }

    public IEnumerable<GraphNode<TValue>> GetSubTreeEnumeration()
    {
        return inNodes.SelectMany(x => x.GetSubTreeEnumeration());
    }
}