using System.Reflection;
using ModeratorBotFrameworkCore.Bot;
using ModeratorBotFrameworkCore.MessagePipeline.Attributes;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;
using UsefulExtensions;

namespace ModeratorBotFrameworkCore.MessagePipeline;

public class DependencyResolver
{
    public IPlugin bot;

    public DependencyResolver(IPlugin bot)
    {
        this.bot = bot;
    }

    private static Dictionary<IMessageHandler, IEnumerable<string>> GetDictionaries(
        List<IMessageHandler> handlers, Func<Type, bool> attributeType)
    {
        var l = handlers.ToDictionary(x => x, x
            => x.GetType()
                .GetCustomAttributes(true)
                .Where(attr => attributeType(attr.GetType()))
                .Select(attr => (BaseAttribute) attr)
                .SelectMany(attr => attr.Parameters)
                .Concat(x.GetType()
                         .GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                         .Where(z => z.FieldType.GetInterface(nameof(IContextAction.IContextAction)) != null || z.FieldType.Name == nameof(IContextAction.IContextAction))
                         .Select(z => z.GetValue(x).GetType())
                         .Select(z => z
                                      .GetCustomAttributes(true)
                                      .Where(attr => attributeType(attr.GetType()))
                                      .Select(attr => (BaseAttribute) attr)
                                      .SelectMany(attr => attr.Parameters))
                         .SelectMany(enumerable => enumerable)));

        return l;
    }

    public bool ResolveBotDependencies(List<IMessageHandler> handlers)
    {
        var deps = GetDictionaries(handlers, type => type == typeof(RequiredBotOptionsAttribute)).Values
            .SelectMany(x => x);
        var imps = bot.GetType().GetMethods().Where(x => x.GetBaseDefinition() != x && !x.IsAbstract)
                      .Select(x => x.Name);
        foreach (var dep in deps)
        {
            if (!imps.Where(x => x == dep).Any())
                throw new ApplicationException($"bot should override {dep}");
        }

        return true;
    }

    public List<IMessageHandler> ResolveDependencies(List<IMessageHandler> handlers)
    {
        var dependencies = GetDictionaries(handlers,
            x => x == typeof(RequiredAttribute) || x == typeof(RequiredOptionalAttribute));
        var implementations = GetDictionaries(handlers,
            x => x == typeof(ImplementingAttribute) || x == typeof(ImplementingOptionalAttribute));


        var dependenciesReversed = GetReversedDictionaries(dependencies);
        var implementationsReversed = GetReversedDictionaries(implementations);

        if (dependenciesReversed.Count == 0)
            return handlers;
        CheckConflictingImplementations((GetDictionaries(handlers,
            x => x == typeof(ImplementingAttribute))));
        CheckUnresolvedDependencies(
            GetReversedDictionaries(GetDictionaries(handlers, x => x == typeof(RequiredAttribute))),
            GetReversedDictionaries(GetDictionaries(handlers, x => x == typeof(ImplementingAttribute))),
            implementations);
        //CheckForCycles(dependencies, implementationsReversed);

        var levels = GetLevelsDictionary(handlers, dependencies, implementationsReversed, implementations);
        ResolveBotDependencies(handlers);
        return handlers.OrderBy(x => levels[x]).ToList();
    }

    private static void CheckUnresolvedDependencies(
        Dictionary<string, IEnumerable<IMessageHandler>> dependenciesReversed,
        Dictionary<string, IEnumerable<IMessageHandler>> implementationsReversed,
        Dictionary<IMessageHandler, IEnumerable<string>> implementations)
    {
        if (dependenciesReversed.Keys.Count > implementationsReversed.Keys.Count)
            throw new ApplicationException("Unresolved dependencies");
    }

    private static void CheckConflictingImplementations(
        Dictionary<IMessageHandler, IEnumerable<string>> implementations)
    {
        if (implementations.Values.SelectMany(x => x).Count() >
            implementations.Values.SelectMany(x => x).Distinct().Count())
            throw new ApplicationException("Conflicting implementation");
    }

    private static Dictionary<IMessageHandler, int> GetLevelsDictionary(IReadOnlyCollection<IMessageHandler> handlers,
                                                                        Dictionary<IMessageHandler, IEnumerable<string>>
                                                                            dependencies,
                                                                        IReadOnlyDictionary<string,
                                                                                IEnumerable<IMessageHandler>>
                                                                            implementationsReversed,
                                                                        Dictionary<IMessageHandler, IEnumerable<string>> implementations)
    {
        var levels = dependencies.Keys
                                 .Where(x => !dependencies[x].Any())
                                 .ToDictionary(x => x, x => 1);
        while (handlers.Count(x => levels.Keys.NotContains(x)) != 0)
        {
            foreach (var messageHandler in handlers.Where(x => levels.NotContainsKey(x)))
            {
                var deps = dependencies[messageHandler]
                           .Where(implementationsReversed.ContainsKey)
                           .Select(x => implementationsReversed[x])
                           .SelectMany(x => x)
                           .Any(x => levels.NotContainsKey(x));
                if (deps)
                {
                    continue;
                }

                if (!implementations[messageHandler].Any())
                {
                    levels[messageHandler] = handlers.Count;
                    continue;
                }

                if (!dependencies[messageHandler].Where(implementationsReversed.ContainsKey).Any())
                {
                    levels[messageHandler] = 1;
                    continue;
                }

                levels[messageHandler] = dependencies[messageHandler]
                                         .Where(implementationsReversed.ContainsKey)
                                         .Select(x => implementationsReversed[x])
                                         .SelectMany(x => x)
                                         .Select(x => levels[x])
                                         .Max() + 1;
            }
        }

        return levels;
    }

    private static void CheckForCycles(Dictionary<IMessageHandler, IEnumerable<string>> dependencies,
                                       Dictionary<string, IEnumerable<IMessageHandler>> implementationsReversed)
    {
        foreach (var startPoint in dependencies.Keys)
        {
            var visited = new HashSet<IMessageHandler> { };
            if (visited.Contains(startPoint) || !dependencies[startPoint].Any())
                continue;
            visited.Add(startPoint);
            var stack = new Stack<IMessageHandler>();
            var pointer = startPoint;

            dependencies[pointer]
                .Where(implementationsReversed.ContainsKey)
                .SelectMany(x => implementationsReversed[x])
                .Distinct()
                .ToList()
                .ForEach(x
                    => stack.Push(x));

            if (stack.Count == 0)
                continue;

            while (stack.Count != 0)
            {
                pointer = stack.Pop();
                if (visited.Contains(pointer))
                    throw new ApplicationException("Cycle Dependency Detected");
                visited.Add(pointer);

                dependencies[pointer]
                    .Where(x=>implementationsReversed.ContainsKey(x))
                    .SelectMany(x => implementationsReversed[x])
                    .Distinct()
                    .ToList()
                    .ForEach(x
                        => stack.Push(x));
            }
        }
    }

    private static Dictionary<string, IEnumerable<IMessageHandler>> GetReversedDictionaries(
        Dictionary<IMessageHandler, IEnumerable<string>> dependencies)
    {
        return dependencies.Values
                           .SelectMany(x => x)
                           .Distinct()
                           .ToDictionary(x => x, x
                               => dependencies.Keys
                                              .Where(z
                                                  => dependencies[z]
                                                      .Contains(x)));
    }
}