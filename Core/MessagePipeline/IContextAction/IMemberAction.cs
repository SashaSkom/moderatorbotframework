using ModeratorBotFrameworkCore.Bot.DTO;

namespace ModeratorBotFrameworkCore.MessagePipeline.IContextAction;

public interface IMemberAction : IContextAction<IEnumerable<Member>>
{
    public void Realize(Dictionary<string, IContextEntry> context, IEnumerable<Member> members);
}   