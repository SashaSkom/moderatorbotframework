namespace ModeratorBotFrameworkCore.MessagePipeline.IContextAction;

public interface IContextAction
{ }

public interface IContextAction<in TContent> : IContextAction
{
    public void Realize(Dictionary<string, IContextEntry> context, TContent members);
}