﻿using DataBasedCollection;
using Handlers.AmiliveCommandHandler;
using Handlers.CaptchaMessageHandler;
using Handlers.CaptchaMessageHandler.Configurators;
using Handlers.DefaultValidator;
using Handlers.DefaultValidator.Commands;
using Handlers.DefaultValidator.Configurators;
using Handlers.KarmaMessageHandler;
using Handlers.KarmaMessageHandler.Commands;
using Handlers.KarmaMessageHandler.Configurators;
using Handlers.KarmaMessageHandler.Data;
using Handlers.MuteMessageHandler;
using Handlers.MuteMessageHandler.Commands;
using Handlers.MuteMessageHandler.Configurators;
using Handlers.MuteMessageHandler.ContextAction;
using Handlers.SayCommandHandler;
using Handlers.WarnHandler;
using ModeratorBotFrameworkCore.MessagePipeline;
using ModeratorBotFrameworkCore.MessagePipeline.IContextAction;
using ModeratorBotFrameworkCore.MessagePipeline.MessageHandler;
using Ninject;
using Telegram.Bot.Types;

namespace Oladushka;

public static class Program
{
    public static async Task Main(string[] args)
    {
        var connectionString = args[0];
        string token = args[1];

        var chatId = long.Parse(args[2]);
        var debugChatId = long.Parse(args[3]);
        
        var bot = new TelegramPlugin.TelegramPlugin(token, message => message.Chat.Id == chatId 
                                                                      || message.Chat.Id == debugChatId);
        var secondPipeline = new MessagePipeline(new DependencyResolver(bot), debugChatId,
            new SayCommandHandler(chatId, "/say"));

        var muteConfig = new MuteHandlerConfigurator("Неверные аргументы команды",
            "Недостаточно привелегий",
            (name, days) => $"@{name}, тебе запрещено говорить {days} дней",
            name => $"{name}, тебя помиловали",
            "/mute",
            "/unmute");
        var privilegeHandlerConfigurator =
            new ValidatorConfigurator((name, priv) => $"@{name}, твой уровень привелегий {priv}",
                "/setpl",
                "/getpl",
                "Неверные аргументы команды");
        var warnConf = new WarnHandlerConfigurator((name, count) => $"@{name}, предупреждение {count}",
            _ => $"История предупреждений очищена",
            "/warn",
            "/clwarn",
            "Укажите аргументы команды");
        var capConf = new CaptchaHandlerConfigurator(s => $"Привет, @{s}.",
            _ => "Верно", _ => "Не верно",
            3,
            4,
            10);
        var carmConf = new KarmaHandlerConfigurator("Неверный формат команды",
            "Пользователь не существует",
            "Нет кармы",
            dto => $"@{dto.Name}, твой уровень кармы {dto.Karma}", (s, i) => $"@{s}, карма +{i}",
            "/carma",
            "/carmarating",
            5
            , 1);

        var container = new StandardKernel();
        container
            .Bind<MessagePipeline>()
            .To<MessagePipeline>();
        container
            .Bind<DependencyResolver>()
            .ToConstant(new DependencyResolver(bot));
        container
            .Bind<long>()
            .ToConstant((long) chatId)
            .WithConstructorArgument("chatId");

        container.Bind<IMessageHandler>().To<KarmaHandler>();
        container.Bind<IMessageHandler>().To<GetKarmaCommand>();
        container.Bind<IMessageHandler>().To<GetKarmaRatingCommand>();
        container.Bind<IKarmaHandlerConfigurator>().ToConstant(carmConf);
        container.Bind<List<string>>().ToConstant(new List<string> {"+"}).WhenInjectedInto<KarmaHandler>();

        container.Bind<IMessageHandler>().ToConstant(new AmILiveCommandHandler("/amilive", "ыавыбвб"));

        container.Bind<IMessageHandler>().To<DefaultValidator>();
        container.Bind<IValidatorConfigurator>().ToConstant(privilegeHandlerConfigurator);
        container.Bind<IMessageHandler>().To<GetPrivilegeLevelCommandHandler>();
        container.Bind<IMessageHandler>().To<SetPrivilegeLevelCommandHandler>();


        container.Bind<IMuteHandlerConfigurator>().ToConstant(muteConfig);
        container.Bind<IMessageHandler>().To<MuteFilter>();
        container.Bind<IMessageHandler>().To<MuteCommandHandler>();
        container.Bind<IMessageHandler>().To<UnmuteCommandHandler>();

        container.Bind<IWarnHandlerConfigurator>().ToConstant(warnConf);
        container.Bind<IMessageHandler>().To<WarnHandler>();

        container.Bind<IMessageHandler>().To<CaptchaFilter>();
        container.Bind<ICaptchaHandlerConfigurator>().ToConstant(capConf);
        container.Bind<IMemberAction>()
                 .ToConstant(new PermanentMuteAction())
                 .WhenInjectedInto<CaptchaFilter>();

        container.Bind<ISafeCollection<MemberPrivilegeEntryDto, long>>()
                 .ToConstant(new CachedDatabasedCollection<MemberPrivilegeEntryDto, long>(connectionString, 3));
        container.Bind<ISafeCollection<MemberWarnDto, long>>()
                 .ToConstant(new CachedDatabasedCollection<MemberWarnDto, long>(connectionString, 3));
        container.Bind<ISafeCollection<MemberCaptchaStatusDto, long>>()
                 .ToConstant(new CachedDatabasedCollection<MemberCaptchaStatusDto, long>(connectionString, 3));
        container.Bind<ISafeCollection<MutedMemberDto, long>>()
                 .ToConstant(new CachedDatabasedCollection<MutedMemberDto, long>(connectionString, 3));
        container.Bind<ISafeCollection<MemberKarmaDto, long>>()
                 .ToConstant(new CachedDatabasedCollection<MemberKarmaDto, long>(connectionString, 3));

        var pipeline = container.Get<MessagePipeline>();

        bot.Connect(pipeline.HandleMessage);
        bot.Connect(secondPipeline.HandleMessage);
        await bot.DoWork();
    }
}